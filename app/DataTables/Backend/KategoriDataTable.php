<?php

namespace App\DataTables\Backend;

use App\Models\Backend\Kategori;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class KategoriDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
        ->editColumn('banner', function(Kategori $kategori){
            return '<img src="'.$kategori->getFirstMediaUrl('banner_image').'" width="50">';
        })
        ->editColumn('icon', function(Kategori $kategori){
            return '<img src="'.$kategori->getFirstMediaUrl('icon_image').'" width="30">';
        })
        ->addColumn('action', 'backend.kategori.datatables_actions')
        ->rawColumns(['action', 'banner', 'icon']);
    }

    
    public function query(Kategori $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'lengthMenu' => [
                    [ 10, 25, 50, 100, -1 ],
                    [ '10', '25', '50', '100', 'All' ]
                ],
                'dom'       => 'Blfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
                'language' => [
                    'sLengthMenu' => '_MENU_',
                    'search' => '_INPUT_',            // Removes the 'Search' field label
                    'searchPlaceholder' => 'Cari Data..'   // Placeholder for the search box
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => 'ID'],
            ['data' => 'name', 'name' => 'name', 'title' => 'Nama'],
            ['data' => 'banner', 'name' => 'banner', 'title' => 'Banner'],
            ['data' => 'icon', 'name' => 'icon', 'title' => 'Icon'],
            ['data' => 'color', 'name' => 'color', 'title' => 'Color'],
            ['data' => 'description', 'name' => 'description', 'title' => 'Desc'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'kategoridatatable_' . time();
    }
}
