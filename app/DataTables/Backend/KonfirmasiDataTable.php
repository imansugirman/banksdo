<?php

namespace App\DataTables\Backend;

use App\Models\Backend\Konfirmasi;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class KonfirmasiDataTable extends DataTable
{
    
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
        ->editColumn('tgl_transfer', function (Konfirmasi $konfirmasi) {
            return $konfirmasi->tgl_transfer ? with(new Carbon($konfirmasi->tgl_transfer))->format('d M Y') : '';
        })
        ->filterColumn('tgl_transfer', function ($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(tgl_transfer,'%d-%m-%Y') like ?", ["%$keyword%"]);
        })
        ->editColumn('created_at', function (Konfirmasi $konfirmasi) {
            return $konfirmasi->created_at ? with(new Carbon($konfirmasi->created_at))->format('d M Y') : '';
        })
        ->filterColumn('created_at', function ($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(created_at,'%d-%m-%Y') like ?", ["%$keyword%"]);
        })
        ->addColumn('action', 'backend.konfirmasi.datatables_actions');
    }

    
    public function query(Konfirmasi $model)
    {
        return $model->newQuery();
    }

    
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'lengthMenu' => [
                    [ 10, 25, 50, 100, -1 ],
                    [ '10', '25', '50', '100', 'All' ]
                ],
                'dom'       => 'Blfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
                'language' => [
                    'sLengthMenu' => '_MENU_',
                    'search' => '_INPUT_',            // Removes the 'Search' field label
                    'searchPlaceholder' => 'Cari Data..'   // Placeholder for the search box
                ]
            ]);
    }

    
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => 'ID'],
            ['data' => 'name', 'name' => 'name', 'title' => 'Nama Donatur'],
            ['data' => 'email', 'name' => 'email', 'title' => 'Email'],
            ['data' => 'nominal', 'name' => 'nominal', 'title' => 'Nominal'],
            ['data' => 'tgl_transfer', 'name' => 'tgl_transfer', 'title' => 'Tgl Transfer'],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Tgl Konfirmasi'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'konfirmasidatatable_' . time();
    }
}
