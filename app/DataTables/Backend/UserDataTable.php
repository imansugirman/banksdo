<?php

namespace App\DataTables\Backend;

use App\User;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class UserDataTable extends DataTable
{
    
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
        ->editColumn('status', function ($user) {

            if ($user->status == 'active') {
                return '<span class="badge bg-green">Active</span>';
            }

            if ($user->status == 'pending') {
                return '<span class="badge bg-orange">Pending</span>';
            }

            if ($user->status == 'nonactive') {
                return '<span class="badge bg-default">Non Aktif</span>';
            }

            if ($user->status == 'reject') {
                return '<span class="badge bg-red">Reject</span>';
            }
            return $user->status;
        })
        ->addColumn('roles', function (User $user) {
            return $user->roles->map(function($roles) {

                if ($roles->name === 'superadmin') {
                    return '<span class="label bg-aqua">Super Admin</span>';
                }

                if ($roles->name === 'admin') {
                    return '<span class="label bg-teal">Admin</span>';
                }

                if ($roles->name === 'staff') {
                    return '<span class="label bg-green">Staff</span>';
                }

                if ($roles->name === 'member') {
                    return '<span class="label bg-maroon">Member</span>';
                }

                if ($roles->name === 'customer') {
                    return '<span class="label bg-primary">Customer</span>';
                }
                return ($roles->name);
            })->implode('<br>');
        })
        ->editColumn('created_at', function (User $user) {
            return $user->created_at ? with(new Carbon($user->created_at))->format('d M Y') : '';
        })
        ->filterColumn('created_at', function ($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(created_at,'%d-%m-%Y') like ?", ["%$keyword%"]);
        })
        ->addColumn('action', 'backend.users.datatables_actions')
        ->rawColumns(['action', 'status', 'roles']);
    }

    
    public function query(User $model)
    {
        return $model->newQuery()->select('*')->with('roles');
    }

    
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'lengthMenu' => [
                        [ 10, 25, 50, 100, -1 ],
                        [ '10', '25', '50', '100', 'All' ]
                ],
                'dom'       => 'Blfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
                'language' => [
                    'sLengthMenu' => '_MENU_',
                    'search' => '_INPUT_',            // Removes the 'Search' field label
                    'searchPlaceholder' => 'Cari Data..'   // Placeholder for the search box
                ]
            ]);
    }

    
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => 'ID'],
            ['data' => 'name', 'name' => 'name', 'title' => 'Name'],
            // ['data' => 'username', 'name' => 'username', 'title' => 'Username'],
            ['data' => 'email', 'name' => 'email', 'title' => 'Email'],
            // ['data' => 'phone', 'name' => 'phone', 'title' => 'No Telp'],
            ['data' => 'status', 'name' => 'status', 'title' => 'Status'],
            ['data' => 'roles', 'name' => 'roles', 'title' => 'Role'],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Created'],
        ];
    }

    
    protected function filename()
    {
        return 'usersdatatable_' . time();
    }
}
