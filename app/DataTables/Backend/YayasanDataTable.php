<?php

namespace App\DataTables\Backend;

use App\Models\Backend\Yayasan;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class YayasanDataTable extends DataTable
{
    
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
        ->editColumn('status', function(Yayasan $yayasan){
            if ($yayasan->status == 'publish') {
                return '<span class="badge bg-green">Publish</span>';
            }

            if ($yayasan->status == 'pending') {
                return '<span class="badge bg-orange">Pending</span>';
            }

            if ($yayasan->status == 'reject') {
                return '<span class="badge bg-red">Reject</span>';
            }
            return $yayasan->status;
        })

        ->editColumn('created_at', function (Yayasan $yayasan) {
            return $yayasan->created_at ? with(new Carbon($yayasan->created_at))->format('d M Y') : '';
        })
        ->filterColumn('created_at', function ($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(created_at,'%d-%m-%Y') like ?", ["%$keyword%"]);
        })

        ->editColumn('creator', function(Yayasan $yayasan){
            if (!empty($yayasan->creator)) {
                return $yayasan->creator->name;
            } else {
                return 'ID User Dihapus';
            }
            
        })

        ->editColumn('foto', function(Yayasan $yayasan){
            return '<img src="'.$yayasan->getFirstMediaUrl('foto_image').'" width="60">';
        })
        ->addColumn('action', 'backend.yayasan.datatables_actions')
        ->rawColumns(['action', 'creator', 'status', 'foto']);
    }

    public function query(Yayasan $yayasan)
    {
        return $yayasan->newQuery()->select('yayasan.*')->with('kategori', 'provinsi', 'users');
    }

   
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax([
                'url'  => route('backend.yayasan.index'),
                'type' => 'GET',
                'data' => "function(data){
                    data.status    = $('select[name=status]').val();
                    data.provinces_id    = $('select[name=provinces_id]').val();
                    data.minDate    = $('input#minDate').val();
                    data.maxDate    = $('input#maxDate').val();
                }"
            ])
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'lengthMenu' => [
                    [ 10, 25, 50, 100, -1 ],
                    [ '10', '25', '50', '100', 'All' ]
                ],
                'dom'       => 'Blfrtip',
                'scrollX'   => true,
                'responsive' => true,
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
                'language' => [
                    'sLengthMenu' => '_MENU_',
                    'search' => '_INPUT_',            // Removes the 'Search' field label
                    'searchPlaceholder' => 'Cari Data..'   // Placeholder for the search box
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => 'ID'],
            ['data' => 'foto', 'name' => 'foto', 'title' => 'Foto'],
            ['data' => 'kategori.name', 'name' => 'kategori.name', 'title' => 'Kategori'],
            ['data' => 'provinsi.name', 'name' => 'provinsi.name', 'title' => 'Provinsi'],
            ['data' => 'name', 'name' => 'name', 'title' => 'Yayasan'],
            ['data' => 'pemilik', 'name' => 'pemilik', 'title' => 'Pemilik'],
            ['data' => 'no_izin', 'name' => 'no_izin', 'title' => 'No Izin'],
            ['data' => 'nama_rekening', 'name' => 'nama_rekening', 'title' => 'Nama Rek'],
            ['data' => 'no_rekening', 'name' => 'no_rekening', 'title' => 'No Rek'],
            ['data' => 'nama_bank', 'name' => 'nama_bank', 'title' => 'Bank'],
            ['data' => 'email_yayasan', 'name' => 'email_yayasan', 'title' => 'Email'],
            ['data' => 'phone', 'name' => 'phone', 'title' => 'No Telp'],
            ['data' => 'status', 'name' => 'status', 'title' => 'Status'],
            ['data' => 'creator', 'name' => 'creator', 'title' => 'Pembuat', 'searchable' => false, 'orderable' => false],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Dibuat'],
        ];
    }

    
    protected function filename()
    {
        return 'yayasandatatable_' . time();
    }
}
