<?php

namespace App\DataTables\Scopes;

use Illuminate\Http\Request;
use Yajra\DataTables\Contracts\DataTableScope;

class YayasanScope implements DataTableScope
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request  = $request;
    }
    
    public function apply($query)
    {
        $filters =  [
            'provinces_id',
            'status',
            'minDate',
            'maxDate'
        ] ;
        foreach ($filters as $field) {
            if ($this->request->has($field)) {
                if($field == 'minDate' || $field == 'maxDate'){
                    if($this->request->get('minDate') !== null || $this->request->get('maxDate') !== null) {
                        $query->whereBetween('created_at', [$this->request->get('minDate'), $this->request->get('maxDate')]);
                    }
                } else {

                    if($this->request->get($field) !== null){
                        $query->where($field, '=', $this->request->get($field));
                    }
                }
            }
        }

        if (auth()->user()->can('super_admin')) {
            return $query;
        } else {
            return $query->where('created_by', auth()->user()->id);
        }
    }
    
}
