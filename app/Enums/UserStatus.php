<?php

namespace App\Enums;

use MadWeb\Enum\Enum;

final class UserStatus extends Enum
{
    const __default = self::Pending;

    const Pending = 'pending';
    const Active = 'active';
    const NonActive = 'nonactive';
    const Reject = 'reject';
}
