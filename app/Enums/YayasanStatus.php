<?php

namespace App\Enums;

use MadWeb\Enum\Enum;

final class YayasanStatus extends Enum
{
    const __default = self::Publish;

    const Publish = 'publish';
    const Pending = 'pending';
    const Reject = 'reject';
}
