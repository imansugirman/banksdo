<?php

namespace App\Exceptions;

use Exception;
use Spatie\Permission\Exceptions\UnauthorizedException;

class PermissionLimit extends Exception
{
    	public function render($request, Throwable $exception)
	{
	    if ($exception instanceof UnauthorizedException) {
	        return response()->json([
	            'responseMessage' => 'Tidak Bisa',
	            'responseStatus'  => 403,
	        ]);
	    }

	    return parent::render($request, $exception);
	}

}
