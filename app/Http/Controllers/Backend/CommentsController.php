<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\Backend\CommentsDataTable;
use App\Http\Requests\Backend;
use App\Http\Requests\Backend\CreateCommentsRequest;
use App\Http\Requests\Backend\UpdateCommentsRequest;
use App\Models\Backend\Comments;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class CommentsController extends AppBaseController
{
    /**
     * Display a listing of the Comments.
     *
     * @param CommentsDataTable $commentsDataTable
     * @return Response
     */
    public function index(CommentsDataTable $commentsDataTable)
    {
        return $commentsDataTable->render('backend.comments.index');
    }

    /**
     * Show the form for creating a new Comments.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.comments.create');
    }

    /**
     * Store a newly created Comments in storage.
     *
     * @param CreateCommentsRequest $request
     *
     * @return Response
     */
    public function store(CreateCommentsRequest $request)
    {
        $input = $request->all();

        /** @var Comments $comments */
        $comments = Comments::create($input);

        Flash::success('Comments saved successfully.');

        return redirect(route('backend.comments.index'));
    }

    /**
     * Display the specified Comments.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Comments $comments */
        $comments = Comments::find($id);

        if (empty($comments)) {
            Flash::error('Comments not found');

            return redirect(route('backend.comments.index'));
        }

        return view('backend.comments.show')->with('comments', $comments);
    }

    /**
     * Show the form for editing the specified Comments.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Comments $comments */
        $comments = Comments::find($id);

        if (empty($comments)) {
            Flash::error('Comments not found');

            return redirect(route('backend.comments.index'));
        }

        return view('backend.comments.edit')->with('comments', $comments);
    }

    /**
     * Update the specified Comments in storage.
     *
     * @param  int              $id
     * @param UpdateCommentsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCommentsRequest $request)
    {
        /** @var Comments $comments */
        $comments = Comments::find($id);

        if (empty($comments)) {
            Flash::error('Comments not found');

            return redirect(route('backend.comments.index'));
        }

        $comments->fill($request->all());
        $comments->save();

        Flash::success('Comments updated successfully.');

        return redirect(route('backend.comments.index'));
    }

    /**
     * Remove the specified Comments from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Comments $comments */
        $comments = Comments::find($id);

        if (empty($comments)) {
            Flash::error('Comments not found');

            return redirect(route('backend.comments.index'));
        }

        $comments->delete();

        Flash::success('Comments deleted successfully.');

        return redirect(route('backend.comments.index'));
    }
}
