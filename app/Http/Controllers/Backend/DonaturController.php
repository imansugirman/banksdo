<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\Backend\DonaturDataTable;
use App\Http\Requests\Backend;
use App\Http\Requests\Backend\CreateDonaturRequest;
use App\Http\Requests\Backend\UpdateDonaturRequest;
use App\Models\Backend\Donatur;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class DonaturController extends AppBaseController
{
    /**
     * Display a listing of the Donatur.
     *
     * @param DonaturDataTable $donaturDataTable
     * @return Response
     */
    public function index(DonaturDataTable $donaturDataTable)
    {
        return $donaturDataTable->render('backend.donatur.index');
    }

    /**
     * Show the form for creating a new Donatur.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.donatur.create');
    }

    /**
     * Store a newly created Donatur in storage.
     *
     * @param CreateDonaturRequest $request
     *
     * @return Response
     */
    public function store(CreateDonaturRequest $request)
    {
        $input = $request->all();

        /** @var Donatur $donatur */
        $donatur = Donatur::create($input);

        Flash::success('Donatur saved successfully.');

        return redirect(route('backend.donatur.index'));
    }

    /**
     * Display the specified Donatur.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Donatur $donatur */
        $donatur = Donatur::find($id);

        if (empty($donatur)) {
            Flash::error('Donatur not found');

            return redirect(route('backend.donatur.index'));
        }

        return view('backend.donatur.show')->with('donatur', $donatur);
    }

    /**
     * Show the form for editing the specified Donatur.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Donatur $donatur */
        $donatur = Donatur::find($id);

        if (empty($donatur)) {
            Flash::error('Donatur not found');

            return redirect(route('backend.donatur.index'));
        }

        return view('backend.donatur.edit')->with('donatur', $donatur);
    }

    /**
     * Update the specified Donatur in storage.
     *
     * @param  int              $id
     * @param UpdateDonaturRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDonaturRequest $request)
    {
        /** @var Donatur $donatur */
        $donatur = Donatur::find($id);

        if (empty($donatur)) {
            Flash::error('Donatur not found');

            return redirect(route('backend.donatur.index'));
        }

        $donatur->fill($request->all());
        $donatur->save();

        Flash::success('Donatur updated successfully.');

        return redirect(route('backend.donatur.index'));
    }

    /**
     * Remove the specified Donatur from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Donatur $donatur */
        $donatur = Donatur::find($id);

        if (empty($donatur)) {
            Flash::error('Donatur not found');

            return redirect(route('backend.donatur.index'));
        }

        $donatur->delete();

        Flash::success('Donatur deleted successfully.');

        return redirect(route('backend.donatur.index'));
    }
}
