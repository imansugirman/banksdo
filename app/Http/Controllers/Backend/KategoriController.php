<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\Backend\KategoriDataTable;
use App\Http\Requests\Backend;
use App\Http\Requests\Backend\CreateKategoriRequest;
use App\Http\Requests\Backend\UpdateKategoriRequest;
use App\Models\Backend\Kategori;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class KategoriController extends AppBaseController
{
    /**
     * Display a listing of the Kategori.
     *
     * @param KategoriDataTable $kategoriDataTable
     * @return Response
     */
    public function index(KategoriDataTable $kategoriDataTable)
    {
        return $kategoriDataTable->render('backend.kategori.index');
    }

    /**
     * Show the form for creating a new Kategori.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.kategori.create');
    }

    /**
     * Store a newly created Kategori in storage.
     *
     * @param CreateKategoriRequest $request
     *
     * @return Response
     */
    public function store(CreateKategoriRequest $request)
    {
        $input = $request->all();

        $kategori = Kategori::create($input);

        if($request->hasFile('banner') && $request->file('banner')->isValid()){
            $kategori->addMediaFromRequest('banner')->toMediaCollection('banner_image');
        }

        if($request->hasFile('icon') && $request->file('icon')->isValid()){
            $kategori->addMediaFromRequest('icon')->toMediaCollection('icon_image');
        }

        Flash::success('Kategori saved successfully.');

        return redirect(route('backend.kategori.index'));
    }

    
    public function show($id)
    {
        $kategori = Kategori::find($id);

        if (empty($kategori)) {
            Flash::error('Kategori not found');

            return redirect(route('backend.kategori.index'));
        }

        return view('backend.kategori.show')->with('kategori', $kategori);
    }

    public function edit($id)
    {
        $kategori = Kategori::find($id);

        if (empty($kategori)) {
            Flash::error('Kategori not found');

            return redirect(route('backend.kategori.index'));
        }

        return view('backend.kategori.edit')->with('kategori', $kategori);
    }

    
    public function update($id, UpdateKategoriRequest $request)
    {
        /** @var Kategori $kategori */
        $kategori = Kategori::find($id);

        if (empty($kategori)) {
            Flash::error('Kategori not found');

            return redirect(route('backend.kategori.index'));
        }

        $kategori->fill($request->all());

        if ($kategori->hasMedia('banner')) {
            $kategori->updateMedia($kategori, 'banner_image'); //images === collection name
        } elseif ($request->hasFile('banner')) {
           $kategori->media()->delete($id);
           $kategori->addMediaFromRequest('banner')->toMediaCollection('banner_image');
        } 

        if ($kategori->hasMedia('icon')) {
            $kategori->updateMedia($kategori, 'icon_image'); //images === collection name
        } elseif ($request->hasFile('icon')) {
           $kategori->media()->delete($id);
           $kategori->addMediaFromRequest('icon')->toMediaCollection('icon_image');
        } 

        $kategori->save();

        Flash::success('Kategori updated successfully.');

        return redirect(route('backend.kategori.index'));
    }

    /**
     * Remove the specified Kategori from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Kategori $kategori */
        $kategori = Kategori::find($id);

        if (empty($kategori)) {
            Flash::error('Kategori not found');

            return redirect(route('backend.kategori.index'));
        }

        $kategori->delete();

        Flash::success('Kategori deleted successfully.');

        return redirect(route('backend.kategori.index'));
    }
}
