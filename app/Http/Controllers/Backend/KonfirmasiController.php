<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\Backend\KonfirmasiDataTable;
use App\Http\Requests\Backend;
use App\Http\Requests\Backend\CreateKonfirmasiRequest;
use App\Http\Requests\Backend\UpdateKonfirmasiRequest;
use App\Models\Backend\Konfirmasi;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class KonfirmasiController extends AppBaseController
{
    
    public function index(KonfirmasiDataTable $konfirmasiDataTable)
    {
        return $konfirmasiDataTable->render('backend.konfirmasi.index');
    }

    
    public function create()
    {
        return view('backend.konfirmasi.create');
    }

    
    public function store(CreateKonfirmasiRequest $request)
    {
        $input = $request->all();

        $konfirmasi = Konfirmasi::create($input);

        Flash::success('Konfirmasi saved successfully.');

        return redirect(route('backend.konfirmasi.index'));
    }

    
    public function show($id)
    {
        $konfirmasi = Konfirmasi::find($id);

        if (empty($konfirmasi)) {
            Flash::error('Konfirmasi not found');

            return redirect(route('backend.konfirmasi.index'));
        }

        return view('backend.konfirmasi.show')->with('konfirmasi', $konfirmasi);
    }

    
    public function edit($id)
    {
        $konfirmasi = Konfirmasi::find($id);

        if (empty($konfirmasi)) {
            Flash::error('Konfirmasi not found');

            return redirect(route('backend.konfirmasi.index'));
        }

        return view('backend.konfirmasi.edit')->with('konfirmasi', $konfirmasi);
    }

   
    public function update($id, UpdateKonfirmasiRequest $request)
    {
        $konfirmasi = Konfirmasi::find($id);

        if (empty($konfirmasi)) {
            Flash::error('Konfirmasi not found');

            return redirect(route('backend.konfirmasi.index'));
        }

        $konfirmasi->fill($request->all());
        $konfirmasi->save();

        Flash::success('Konfirmasi updated successfully.');

        return redirect(route('backend.konfirmasi.index'));
    }


    public function destroy($id)
    {
        /** @var Konfirmasi $konfirmasi */
        $konfirmasi = Konfirmasi::find($id);

        if (empty($konfirmasi)) {
            Flash::error('Konfirmasi not found');

            return redirect(route('backend.konfirmasi.index'));
        }

        $konfirmasi->delete();

        Flash::success('Konfirmasi deleted successfully.');

        return redirect(route('backend.konfirmasi.index'));
    }
}
