<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\Backend\ProvinsiDataTable;
use App\Http\Requests\Backend;
use App\Http\Requests\Backend\CreateProvinsiRequest;
use App\Http\Requests\Backend\UpdateProvinsiRequest;
use App\Models\Backend\Provinsi;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ProvinsiController extends AppBaseController
{
    
    public function index(ProvinsiDataTable $provinsiDataTable)
    {
        return $provinsiDataTable->render('backend.provinsi.index');
    }

   
    public function create()
    {
        return view('backend.provinsi.create');
    }

    
    public function store(CreateProvinsiRequest $request)
    {
        $input = $request->all();

        $provinsi = Provinsi::create($input);

        Flash::success('Provinsi saved successfully.');

        return redirect(route('backend.provinsi.index'));
    }

    
    public function show($id)
    {
        /** @var Provinsi $provinsi */
        $provinsi = Provinsi::find($id);

        if (empty($provinsi)) {
            Flash::error('Provinsi not found');

            return redirect(route('backend.provinsi.index'));
        }

        return view('backend.provinsi.show')->with('provinsi', $provinsi);
    }

    
    public function edit($id)
    {
        /** @var Provinsi $provinsi */
        $provinsi = Provinsi::find($id);

        if (empty($provinsi)) {
            Flash::error('Provinsi not found');

            return redirect(route('backend.provinsi.index'));
        }

        return view('backend.provinsi.edit')->with('provinsi', $provinsi);
    }

    
    public function update($id, UpdateProvinsiRequest $request)
    {
        $provinsi = Provinsi::find($id);

        if (empty($provinsi)) {
            Flash::error('Provinsi not found');

            return redirect(route('backend.provinsi.index'));
        }

        $provinsi->fill($request->all());
        

        $provinsi->save();

        Provinsi::all()->each->save();

        Flash::success('Provinsi updated successfully.');

        return redirect(route('backend.provinsi.index'));
    }

    
    public function destroy($id)
    {
        /** @var Provinsi $provinsi */
        $provinsi = Provinsi::find($id);

        if (empty($provinsi)) {
            Flash::error('Provinsi not found');

            return redirect(route('backend.provinsi.index'));
        }

        $provinsi->delete();

        Flash::success('Provinsi deleted successfully.');

        return redirect(route('backend.provinsi.index'));
    }
}
