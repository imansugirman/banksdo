<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\Backend\RoleDataTable;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Backend;
use App\Http\Requests\Backend\CreateRoleRequest;
use App\Http\Requests\Backend\UpdateRoleRequest;
use App\Models\Backend\Permission;
use App\Models\Backend\Role;
use Flash;
use Response;

class RoleController extends AppBaseController
{
    
    public function index(RoleDataTable $roleDataTable)
    {
        return $roleDataTable->render('backend.roles.index');
    }

    
    public function create()
    {

        $permissions = Permission::get();

        return view('backend.roles.create', compact('permissions'));
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param CreateRoleRequest $request
     *
     * @return Response
     */
    public function store(CreateRoleRequest $request)
    {
        $input = $request->all();

        $role = Role::create($input);

        $role->givePermissionTo([$request->input('permissions')]);

        Flash::success('Role saved successfully.');

        return redirect(route('backend.roles.index'));
    }

    /**
     * Display the specified Role.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $role = Role::find($id);

        $permissions = Permission::all();
        $permissionId = $permissions->pluck('id')->implode('');

        $rolePermission = $role->permissions;


        if (empty($role)) {
            Flash::error('Role not found');

            return redirect(route('backend.roles.index'));
        }

        return view('backend.roles.show', compact('permissions', 'permissionId', 'rolePermission'))->with('role', $role);
    }

    
    public function edit($id)
    {
        $role = Role::find($id);

        $permissions = Permission::get();

        if (empty($role)) {
            Flash::error('Role not found');

            return redirect(route('backend.roles.index'));
        }

        return view('backend.roles.edit', compact('permissions'))->with('role', $role);
    }

    /**
     * Update the specified Role in storage.
     *
     * @param  int              $id
     * @param UpdateRoleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoleRequest $request)
    {
        /** @var Role $role */
        $role = Role::find($id);

        if (empty($role)) {
            Flash::error('Role not found');

            return redirect(route('backend.roles.index'));
        }

        $role->syncPermissions([$request->input('permissions')]);

        $role->fill($request->all());
        $role->save();

        Flash::success('Role updated successfully.');

        return redirect(route('backend.roles.index'));
    }

    /**
     * Remove the specified Role from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Role $role */
        $role = Role::find($id);

        if (empty($role)) {
            Flash::error('Role not found');

            return redirect(route('backend.roles.index'));
        }

        $role->delete();

        Flash::success('Role deleted successfully.');

        return redirect(route('backend.roles.index'));
    }
}
