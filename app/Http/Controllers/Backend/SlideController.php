<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\Backend\SlideDataTable;
use App\Http\Requests\Backend;
use App\Http\Requests\Backend\CreateSlideRequest;
use App\Http\Requests\Backend\UpdateSlideRequest;
use App\Models\Backend\Slide;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class SlideController extends AppBaseController
{
    /**
     * Display a listing of the Slide.
     *
     * @param SlideDataTable $slideDataTable
     * @return Response
     */
    public function index(SlideDataTable $slideDataTable)
    {
        return $slideDataTable->render('backend.slide.index');
    }

    /**
     * Show the form for creating a new Slide.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.slide.create');
    }

    /**
     * Store a newly created Slide in storage.
     *
     * @param CreateSlideRequest $request
     *
     * @return Response
     */
    public function store(CreateSlideRequest $request)
    {
        $input = $request->all();

        /** @var Slide $slide */
        $slide = Slide::create($input);
        $slide->uploadImage(request()->file('image'), 'image');


        Flash::success('Slide saved successfully.');

        return redirect(route('backend.slide.index'));
    }

    /**
     * Display the specified Slide.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Slide $slide */
        $slide = Slide::find($id);

        if (empty($slide)) {
            Flash::error('Slide not found');

            return redirect(route('backend.slide.index'));
        }

        return view('backend.slide.show')->with('slide', $slide);
    }

    /**
     * Show the form for editing the specified Slide.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Slide $slide */
        $slide = Slide::find($id);

        if (empty($slide)) {
            Flash::error('Slide not found');

            return redirect(route('backend.slide.index'));
        }

        return view('backend.slide.edit')->with('slide', $slide);
    }

    /**
     * Update the specified Slide in storage.
     *
     * @param  int              $id
     * @param UpdateSlideRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSlideRequest $request)
    {
        /** @var Slide $slide */
        $slide = Slide::find($id);

        if (empty($slide)) {
            Flash::error('Slide not found');

            return redirect(route('backend.slide.index'));
        }

        $slide->fill($request->all());
        $slide->save();

        Flash::success('Slide updated successfully.');

        return redirect(route('backend.slide.index'));
    }

    /**
     * Remove the specified Slide from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Slide $slide */
        $slide = Slide::find($id);

        if (empty($slide)) {
            Flash::error('Slide not found');

            return redirect(route('backend.slide.index'));
        }

        $slide->delete();

        Flash::success('Slide deleted successfully.');

        return redirect(route('backend.slide.index'));
    }
}
