<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\Backend\UserDataTable;
use App\Enums\UserStatus;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Backend;
use App\Http\Requests\Backend\CreateUserRequest;
use App\Http\Requests\Backend\UpdateUserRequest;
use App\Models\Backend\Role;
use App\User;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Response;

class UserController extends AppBaseController
{
    
    public function index(UserDataTable $userDataTable)
    {
        return $userDataTable->render('backend.users.index');
    }

    
    public function create()
    {
        $status = UserStatus::labels();

        $roles = Role::pluck('name', 'id');

        return view('backend.users.create', compact('status', 'roles'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $this->handlePasswordInput($request);

        $this->validate($request, [
            'status' => ['required', UserStatus::rule()],
        ]);

        $input = $request->all();

        $user = User::create($input);



        if($request->hasFile('avatar') && $request->file('avatar')->isValid()){
            $user->addMediaFromRequest('avatar')->toMediaCollection('avatars');
        }

        $user->assignRole([$request->input('roles')]);

        Flash::success('User saved successfully.');

        return redirect(route('backend.users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('backend.users.index'));
        }

        return view('backend.users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var User $user */
        $user = User::find($id);

        $roles = Role::pluck('name', 'id');

        $status = UserStatus::labels();

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('backend.users.index'));
        }

        return view('backend.users.edit', compact('status', 'roles'))->with('user', $user);
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int              $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $this->handlePasswordInput($request);
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('User not found');
            return redirect(route('backend.users.index'));
        }

        $user->fill($request->all());

        if ($user->hasMedia('avatar')) {
             $user->updateMedia($user, 'avatars'); //images === collection name
        } elseif ($request->hasFile('avatar')) {
            $user->media()->delete($id);
            $user->addMediaFromRequest('avatar')
            ->toMediaCollection('avatars');
        } 

        $user->syncRoles([$request->input('roles')]);
        
        $user->save();


        Flash::success('User updated successfully.');

        return redirect(route('backend.users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('backend.users.index'));
        }

        $user->delete();

        Flash::success('User deleted successfully.');

        return redirect(route('backend.users.index'));
    }

    protected function handlePasswordInput(Request $request)
    {
        // Remove fields not present on the user.
        $request->request->remove('password_confirmation');

        // Encrypt password if specified.
        if ($request->input('password')) {
            $request->request->set('password', Hash::make($request->input('password')));
        } else {
            $request->request->remove('password');
        }

    }
}
