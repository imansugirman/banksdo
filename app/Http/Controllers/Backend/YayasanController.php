<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\Backend\YayasanDataTable;
use App\DataTables\Scopes\YayasanScope;
use App\Enums\YayasanStatus;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Backend;
use App\Http\Requests\Backend\CreateYayasanRequest;
use App\Http\Requests\Backend\UpdateYayasanRequest;
use App\Mail\NotifikasiAdmin;
use App\Models\Backend\Kategori;
use App\Models\Backend\Provinsi;
use App\Models\Backend\Yayasan;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Response;
use Auth;

class YayasanController extends AppBaseController
{

    public function __construct()
    {
        $this->middleware(['auth', 'permission:list_yayasan'])->only('index');
        $this->middleware(['auth', 'permission:create_yayasan'])->only(['create', 'store']);
        $this->middleware(['auth', 'permission:update_yayasan'])->only(['update', 'edit']);
        $this->middleware(['auth', 'permission:show_yayasan'])->only('show');
    }

    
    public function index(YayasanDataTable $yayasanDataTable, Request $request)
    {
        $yayasan = Yayasan::get();
        $status = YayasanStatus::labels();
        $provinsi = Provinsi::get();

        return $yayasanDataTable->addScope(new YayasanScope($request))->render('backend.yayasan.index', compact('yayasan', 'status', 'provinsi'));
    }

    
    public function create()
    {
        if (Auth::check()) {
            $kategori = Kategori::pluck('name', 'id');
            $provinces = Provinsi::pluck('name', 'id');
            $status = YayasanStatus::labels();

            return view('backend.yayasan.create', compact('kategori', 'provinces', 'status'));
        } else {
            Flash::error('Anda Harus Login Dulu');
            return redirect()->route('login');
        }
        
        
    }

    public function store(CreateYayasanRequest $request)
    {
        $input = $request->all();

        $yayasan = Yayasan::create($input);

        if($request->hasFile('foto') && $request->file('foto')->isValid()){
            $yayasan->addMediaFromRequest('foto')->toMediaCollection('foto_image');
        }

        Mail::to(['bankdonation.business@gmail.com'])->send(new NotifikasiAdmin($yayasan));


        Flash::success('Yayasan saved successfully.');

        return redirect(route('backend.yayasan.index'));
    }

    
    public function show($id)
    {
        $yayasan = Yayasan::find($id);

        if (empty($yayasan)) {
            Flash::error('Yayasan not found');

            return redirect(route('backend.yayasan.index'));
        }

        return view('backend.yayasan.show')->with('yayasan', $yayasan);
    }

    
    public function edit($id)
    {
        $yayasan = Yayasan::find($id);

        $kategori = Kategori::pluck('name', 'id');
        $provinces = Provinsi::pluck('name', 'id');
        $status = YayasanStatus::labels();
        $creator = auth()->user()->id;

        if (empty($yayasan)) {
            Flash::error('Yayasan not found');

            return redirect(route('backend.yayasan.index'));
        }

        return view('backend.yayasan.edit', compact('kategori', 'provinces', 'status', 'creator'))->with('yayasan', $yayasan);
    }

    
    public function update($id, UpdateYayasanRequest $request)
    {
        $yayasan = Yayasan::find($id);

        if (empty($yayasan)) {
            Flash::error('Yayasan not found');

            return redirect(route('backend.yayasan.index'));
        }

        $yayasan->fill($request->all());

        if ($yayasan->hasMedia('foto')) {
            $yayasan->updateMedia($yayasan, 'foto_image'); //images === collection name
       } elseif ($request->hasFile('foto')) {
           $yayasan->media()->delete($id);
           $yayasan->addMediaFromRequest('foto')->toMediaCollection('foto_image');
       } 

        $yayasan->save();

        Flash::success('Yayasan updated successfully.');

        return redirect(route('backend.yayasan.index'));
    }

    /**
     * Remove the specified Yayasan from storage.
     *
     * @param  int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Yayasan $yayasan */
        $yayasan = Yayasan::find($id);

        if (empty($yayasan)) {
            Flash::error('Yayasan not found');

            return redirect(route('backend.yayasan.index'));
        }

        $yayasan->delete();

        Flash::success('Yayasan deleted successfully.');

        return redirect(route('backend.yayasan.index'));
    }
}
