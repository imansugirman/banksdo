<?php

namespace App\Http\Controllers;

use App\Enums\YayasanStatus;
use App\Models\Backend\Kategori;
use App\Models\Backend\Provinsi;
use App\Models\Backend\Slide;

use App\Models\Backend\Yayasan;
use App\Models\Donatur;
use Newsletter;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Str;
use Laracasts\Flash\Flash;

class FrontController extends Controller
{

    public function index()
    {

        $published = YayasanStatus::Publish;
        $kategori = Kategori::all();
        $yayasan = Yayasan::where('status', 'publish')->paginate(16);
        $provinsi = Provinsi::all();

        $provinsimobile = Provinsi::all();
        $slides = Slide::orderBy('created_at', 'asc')->get();

        return view('fronts.index', [
            'kategori' => $kategori,
            'yayasan' => $yayasan,
            'provinsi' => $provinsi,
            'slides' => $slides,
            'provinsimobile' => $provinsimobile
        ]);
    }

    public function search(Request $request)
    {
        $published = YayasanStatus::Publish;

        $kategori = Kategori::all();
        $provinsi = Provinsi::all();
        $provinsimobile = Provinsi::all();

        $yayasan = Yayasan::where('status', $published)
                    ->when($request->q, function ($query) use ($request) {
                        $query->where('name', 'LIKE', "%{$request->q}%");
                    })->get();
        if(!$yayasan->isEmpty()) {
           return view('fronts.search', ['yayasan' => $yayasan, 'kategori' => $kategori, 'provinsi' => $provinsi, 'q' => $request->q, 'provinsimobile' => $provinsimobile]);
        }  else {
           return view('fronts.tidakditemukan', [
            'yayasan' => $yayasan,
            'kategori' => $kategori,
            'provinsi' => $provinsi,
            'provinsimobile' => $provinsimobile,
           ]);
        }

        // return view('', compact());
    }

    public function searchProvinsi(Request $request)
    {
        $published = YayasanStatus::Publish;

        $kategori = Kategori::all();
        $provinsi = Provinsi::all();
        $provinsimobile = Provinsi::all();

        $yayasan = Yayasan::where('status', $published)
            ->when($request->input('id'), function ($query) use ($request) {
                $query->where('provinces_id', '=', $request->input('id'));
            })->get();
            // dd($request->input('id'));
        if(!$yayasan->isEmpty()) {
            return view('fronts.searchprovinsi', [
                // 'id' => $request->input('id'),
                'yayasan' => $yayasan,
                'kategori' => $kategori,
                'provinsi' => $provinsi,
                'provinsimobile' => $provinsimobile,
            ]);
        }  else {
            return view('fronts.tidakditemukan', [
                'yayasan' => $yayasan,
                'kategori' => $kategori,
                'provinsi' => $provinsi,
                'provinsimobile' => $provinsimobile,
            ]);
        }

    }




    public function detail($slug)
    {
        $published = YayasanStatus::Publish;
        $yayasan = Yayasan::where('slug', $slug)->where('status', $published)->get();


        if (empty($yayasan)) {
            Flash::error('Yayasan not found');

            return redirect(route('backend.yayasan.index'));
        }

        $yayasanID = $yayasan->pluck('id')->implode('');
        $provinsimobile = Provinsi::all();

        $kategori = Kategori::all();
        $provinsi = Provinsi::all();
        $get_yayasan = $yayasan->pluck('provinces_id')->implode('');
        $get_provinsi = Provinsi::where('id', $get_yayasan)->first();
        // dd($get_provinsi->id);
        $slug_provinsi = Str::slug($get_provinsi->name, '-');

        $getyayasanrelated = $yayasan->pluck('kategori_id')->implode('');

        $yayasanFirst = Yayasan::where('slug', $slug)->where('status', $published)->first();
        $comment = $yayasanFirst->comments()->paginate(5);

        // dd($yayasanID);

        $related = Yayasan::where('kategori_id', $getyayasanrelated)->whereNotIn('id', [$yayasanID])->limit(8)->get();

        // dd($related);

        return view('fronts.detail')->with([
            'yayasan' => $yayasan,
            'provinsi' => $provinsi,
            'kategori' => $kategori,
            'slug_provinsi' => $slug_provinsi,
            'related' => $related,
            'provinsimobile' => $provinsimobile,
            'comment' => $comment,
        ]);
    }

    public function comment($slug, Request $request) 
    {
        $published = YayasanStatus::Publish;
        $yayasan = Yayasan::where('slug', $slug)->where('status', $published)->first();


        if (empty($yayasan)) {
            Flash::error('Yayasan not found');

            return redirect(route('backend.yayasan.index'));
        }
        if (!empty($request->input('comment'))) {
            $yayasan->comment($request->input('comment'));

            Flash::success('Terimakasih');

            return redirect()->back();
        } else {
            Flash::error('Isi Komentar Kosong');

            return redirect()->back();
        }

    }

    public function donatur()
    {
        $published = YayasanStatus::Publish;

        $kategori = Kategori::all();
        $yayasan = Yayasan::where('status', $published)->get();
        $provinsi = Provinsi::all();
        $provinsimobile = Provinsi::all();

        return view('fronts.donatur', compact('kategori', 'yayasan', 'provinsi', 'provinsimobile'));
    }

    public function donaturstore(Request $request)
    {

        $input = [
            'nama_lengkap'    => $request->input('nama_lengkap'),
            'email'         => $request->input('email'),
            'phone'         => $request->input('phone'),
            'asuransi'      => $request->input('asuransi'),
        ];

        $donatur = Donatur::create($input);

        Newsletter::subscribe($request->input('email'), ['FNAME'=> $request->input('name')], 'subscribers');

        Flash::success('Pendaftaran Menjadi Donatur Berhasil');

        return redirect(route('front'));

        // return view('fronts.donatur', compact('kategori', 'yayasan', 'provinsi', 'provinsimobile'));
    }

    public function kategori($slug)
    {
        $published = YayasanStatus::Publish;
        $kategori = Kategori::where('slug', $slug)->get();
        $get_kategori = $kategori->pluck('id')->implode('');
        $yayasan = Yayasan::where('status', $published)->where('kategori_id', $get_kategori)->paginate(16);
//        $yayasan = Yayasan::where('status', 'publish');
        $provinsi = Provinsi::all();
        $provinsimobile = Provinsi::all();

        return view('fronts.kategori', compact('kategori', 'yayasan', 'provinsi'))->with([
            'kategori' => $kategori,
            'yayasan' => $yayasan,
            'provinsi' => $provinsi,
            'provinsimobile' => $provinsimobile,
        ]);
    }

    public function provinsi($slug)
    {
        $published = YayasanStatus::Publish;
        $getProvinsi = Provinsi::where('slug', $slug)->pluck('id')->implode('');
        $yayasan = Yayasan::where('status', $published)->where('provinces_id', $getProvinsi)->get();


        $kategori = Kategori::where('slug', $slug)->get();
        $provinsi = Provinsi::all();
        $provinsimobile = Provinsi::all();

        return view('fronts.provinsi', compact('yayasan', 'getProvinsi'))->with([
            'kategori' => $kategori,
            'yayasan' => $yayasan,
            'getProvinsi' => $getProvinsi,
            'provinsi' => $provinsi,
            'provinsimobile' => $provinsimobile,
        ]);
    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
