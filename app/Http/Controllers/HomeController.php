<?php

namespace App\Http\Controllers;

use App\User;
use GoogleMaps\GoogleMaps;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Backend\Yayasan;
use App\Models\Backend\Kategori;
use App\Models\Backend\Provinsi;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $kategori = Kategori::all();
        $yayasan = Yayasan::all();
        $provinsi = Provinsi::all();

        $yayasan = auth()->user()->hasRole('yayasan');
        $donatur = auth()->user()->hasRole('donatur');
        $superadmin = auth()->user()->hasRole('superadmin');

        $users_without_any_roles = User::doesntHave('roles')->get();

        if ($donatur) {
            return redirect(route('front'));
            // return redirect(route('fronts.index', compact('kategori', 'yayasan', 'provinsi')));
        } elseif($yayasan) {
            return redirect(route('backend.yayasan.create'));
            // return redirect(route(''));
        } elseif($superadmin) {
            return redirect(route('backend.users.index')); 
        } 
    
    }
}
