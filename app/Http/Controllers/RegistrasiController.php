<?php

namespace App\Http\Controllers;

use App\User;
use Newsletter;
use App\Enums\UserStatus;
use Laracasts\Flash\Flash;
use App\Models\Backend\Role;
use App\Models\Backend\Yayasan;
use App\Models\Backend\Kategori;
use App\Models\Backend\Provinsi;
use App\Enums\YayasanStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Mail\NotifikasiAdmin;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class RegistrasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::whereIn('id', [4,5])->pluck('name', 'id');
        $kategori = Kategori::pluck('name', 'id');
        $provinces = Provinsi::pluck('name', 'id');
        $status = YayasanStatus::labels();
        return view('fronts.register', compact('roles', 'provinces', 'kategori', 'status'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    	// dd($request->all());

        $input = [
            'name'          => $request->input('name'),
            'email'         => $request->input('email'),
            'status'        => 'pending',
            'password'      => Hash::make($request->input('password')),
        ];

        $user = User::create($input);


        $user->assignRole([$request->input('roles')]);

        $donatur = $user->roles->pluck('name')->implode('');

        $inputyayasan = [
            'kategori_id' => $request->input('kategori_id'), 
            'provinces_id' => $request->input('provinces_id'), 
            'user_id' => $user->id, 
            'name' => $request->input('name'),
            'slug' => Str::slug($request->input('name'), '-'), 
            'pemilik' => $request->input('pemilik'),
            'no_izin' => $request->input('no_izin'),
            'nama_rekening' => $request->input('nama_rekening'),
            'no_rekening' => $request->input('no_rekening'),
            'nama_bank' => $request->input('nama_bank'),
            'email_yayasan' => $request->input('email_yayasan'),
            'phone' => $request->input('phone'),
            'alamat' => $request->input('alamat'),
            'web' => $request->input('web'),
            'status' => YayasanStatus::Pending(),
            'foto' => $request->input('foto'),
            'created_by' => $user->id,
        ];

        $yayasan = Yayasan::create($inputyayasan);

        if($request->hasFile('foto') && $request->file('foto')->isValid()){
            $yayasan->addMediaFromRequest('foto')->toMediaCollection('foto_image');
        }

        Mail::to(['bankdonation.business@gmail.com'])->send(new NotifikasiAdmin($yayasan));


        if ($donatur == 'donatur') {
            Newsletter::subscribe($request->input('email'), ['FNAME'=> $request->input('name')], 'subscribers');

            Flash::success('Pendaftaran Berhasil');

            return redirect(route('front'));
        } else {

            Flash::success('Pendaftaran Sebagai Yayasan Berhasil, Silahkan Login');

            return redirect(route('login'));
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
