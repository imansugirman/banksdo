<?php

namespace App\Http\Resources;

use App\Http\Resources\PengaduanItem;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PengaduanCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'data' => PengaduanItem::collection($this->collection)
        ];
        // return parent::toArray($request);
    }
}
