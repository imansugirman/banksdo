<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PengaduanItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // $get_kategori = Pengaduan::get();
        // $get_koka = $get_kategori->kategori
        return [
            'judul' => $this->judul,
            'kategori' => $this->kategori->name,
            'deskripsi' => $this->deskripsi,
            'foto' => $this->getFirstMediaUrl('media'),
        ];
        // return parent::toArray($request);
    }
}
