<?php

namespace App\Models\Backend;

use Eloquent as Model;

/**
 * Class Comments
 * @package App\Models\Backend
 * @version November 4, 2020, 4:52 pm UTC
 *
 * @property string $commentable_type
 * @property integer $commentable_id
 * @property string $comment
 * @property string $ip_address
 * @property string $user_agent
 */
class Comments extends Model
{

    public $table = 'comments';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'commentable_type',
        'commentable_id',
        'comment',
        'ip_address',
        'user_agent'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'commentable_type' => 'string',
        'commentable_id' => 'integer',
        'comment' => 'string',
        'ip_address' => 'string',
        'user_agent' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'commentable_type' => 'required|string|max:255',
        'commentable_id' => 'required',
        'comment' => 'required|string',
        'ip_address' => 'nullable|string|max:45',
        'user_agent' => 'nullable|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    
}
