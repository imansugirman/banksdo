<?php

namespace App\Models\Backend;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Donatur
 * @package App\Models\Backend
 * @version July 2, 2020, 3:52 pm UTC
 *
 * @property string $nama_lengkap
 * @property string $email
 * @property string $phone
 * @property boolean $asuransi
 */
class Donatur extends Model
{
    use SoftDeletes;

    public $table = 'donatur';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nama_lengkap',
        'email',
        'phone',
        'asuransi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_lengkap' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'asuransi' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama_lengkap' => 'required',
        'email' => 'required',
        'phone' => 'required',
        'asuransi' => 'required'
    ];

    
}
