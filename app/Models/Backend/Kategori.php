<?php

namespace App\Models\Backend;

use App\Models\Backend\Yayasan;
use Cviebrock\EloquentSluggable\Sluggable;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Kategori extends Model implements HasMedia
{
    use SoftDeletes, InteractsWithMedia, Sluggable;

    public $table = 'kategori';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    
    public $fillable = ['name', 'banner', 'description', 'icon', 'color'];

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'banner' => 'string',
        'description' => 'string',
        'icon' => 'string',
        'color' => 'string'
    ];


    public static $rules = [
        'name' => 'required',
        'icon' => 'required'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
              ->width(368)
              ->height(232)
              ->sharpen(10);
        
        $this->addMediaConversion('icon')
              ->width(150)
              ->height(150)
              ->sharpen(10);
    }

    
    public function yayasan()
    {
        return $this->hasMany(Yayasan::class, 'kategori_id');
    }
    

    
}
