<?php

namespace App\Models\Backend;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Konfirmasi extends Model implements HasMedia
{
    use SoftDeletes, InteractsWithMedia;

    public $table = 'konfirmasi';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = ['name', 'email', 'nominal', 'tgl_transfer'];

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'nominal' => 'string',
        'tgl_transfer' => 'date'
    ];

    public static $rules = [
        'email' => 'required',
        'tgl_transfer' => 'required'
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
              ->width(368)
              ->height(600)
              ->sharpen(10);
    }

    
}
