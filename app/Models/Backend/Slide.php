<?php

namespace App\Models\Backend;

use Eloquent as Model;
use QCod\ImageUp\HasImageUploads;

class Slide extends Model
{
    use HasImageUploads;

    public $table = 'slide';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    // protected $dates = ['deleted_at'];



    public $fillable = [
        'image',
        'title',
        'description',
        'text',
        'link'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'image' => 'string',
        'title' => 'string',
        'description' => 'string',
        'text' => 'string',
        'link' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        // 'image' => 'required|string|max:255',
        'title' => 'nullable|string|max:255',
        'description' => 'nullable|string|max:255',
        'text' => 'nullable|string|max:255',
        'link' => 'nullable|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    protected static $imageFields = [
        'image' => [

            'crop' => false,
            
            // what disk you want to upload, default config('imageup.upload_disk')
            'disk' => 'public',
            
            // a folder path on the above disk, default config('imageup.upload_directory')
            'path' => 'slide',
            
            // placeholder image if image field is empty
            // 'placeholder' => '/images/avatar-placeholder.svg',
            
            // validation rules when uploading image
            'rules' => 'image|max:4000',
            
            // override global auto upload setting coming from config('imageup.auto_upload_images')
            'auto_upload' => true,
            
            // if request file is don't have same name, default will be the field name
            'file_input' => 'image',
            
        ]

    ];

    
}
