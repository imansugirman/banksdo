<?php

namespace App\Models\Backend;

use App\Enums\YayasanStatus;
use App\Models\Backend\Kategori;
use App\Models\Backend\Provinsi;
use App\User;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Wildside\Userstamps\Userstamps;
use Orkhanahmadov\LaravelCommentable\Commentable;

class Yayasan extends Model implements HasMedia
{
    use SoftDeletes, Userstamps, InteractsWithMedia, Sluggable, SluggableScopeHelpers, Commentable;

    public $table = 'yayasan';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const DELETED_BY = 'deleted_by';

    protected $dates = ['deleted_at'];

    public $fillable = ['kategori_id', 'provinces_id', 'user_id', 'slug', 'name', 'pemilik', 'no_izin', 'nama_rekening', 'no_rekening', 'nama_bank', 'email_yayasan', 'phone', 'alamat', 'status', 'foto', 'web'];


    protected $casts = [
        'id' => 'integer',
        'kategori_id' => 'integer',
        'provinces_id' => 'integer',
        'user_id' => 'integer',
        'name' => 'string',
        'pemilik' => 'string',
        'no_izin' => 'string',
        'nama_rekening' => 'string',
        'no_rekening' => 'string',
        'nama_bank' => 'string',
        'email_yayasan' => 'string',
        'phone' => 'string',
        'alamat' => 'string',
        'web' => 'string',
        'status' => YayasanStatus::class,
    ];

    
    public static $rules = [
        'kategori_id' => 'required',
        'provinces_id' => 'required',
        'user_id' => 'required',
        'name' => 'required',
        'pemilik' => 'required',
        'no_izin' => 'required',
        'nama_rekening' => 'required',
        'no_rekening' => 'required',
        'nama_bank' => 'required',
        'email_yayasan' => 'required',
        'phone' => 'required',
        'alamat' => 'required'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
              ->width(300)
              ->height(220)
              ->sharpen(1);
    }


    
    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'kategori_id');
    }

    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'provinces_id');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
}
