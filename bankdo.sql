-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: bankdo_m8i22
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategori`
--

DROP TABLE IF EXISTS `kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategori` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategori`
--

LOCK TABLES `kategori` WRITE;
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
INSERT INTO `kategori` VALUES (1,'Anak Yatim','anak-yatim',NULL,NULL,'/tmp/phpj57FA1','#dddd','2020-06-03 22:26:36','2020-06-05 23:11:48',NULL),(2,'Pembangunan','pembangunan',NULL,NULL,'/tmp/phpNII9HC','#fff','2020-06-03 22:26:48','2020-06-05 23:11:38',NULL),(3,'Kesehatan','kesehatan',NULL,NULL,'/tmp/phpBYU8Sr','#fff','2020-06-03 22:27:09','2020-06-05 23:11:25',NULL),(4,'Pendidikan','pendidikan',NULL,NULL,'/tmp/phpv24CWb','#ffff','2020-06-03 22:27:28','2020-06-05 23:11:10',NULL),(5,'Rumah Jompo','rumah-jompo',NULL,NULL,'/tmp/phpJSARbb','#fff','2020-06-03 22:27:47','2020-06-05 23:11:02',NULL),(6,'Sosial Keamanan','sosial-keamanan',NULL,NULL,'/tmp/phpZLeP4W','#fff','2020-06-03 22:28:02','2020-06-05 23:10:32',NULL);
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `konfirmasi`
--

DROP TABLE IF EXISTS `konfirmasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `konfirmasi` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nominal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_transfer` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `konfirmasi`
--

LOCK TABLES `konfirmasi` WRITE;
/*!40000 ALTER TABLE `konfirmasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `konfirmasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conversions_disk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` bigint(20) unsigned NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `responsive_images` json NOT NULL,
  `order_column` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` VALUES (13,'App\\Models\\Backend\\Kategori',6,'256519ca-706f-4ee6-ac00-281eed4c5bf3','icon_image','button-sosial-keamanan (1)','button-sosial-keamanan-(1).png','image/png','public','public',14810,'[]','{\"generated_conversions\": {\"icon\": true, \"thumb\": true}}','[]',13,'2020-06-05 23:10:31','2020-06-05 23:10:32'),(15,'App\\Models\\Backend\\Kategori',5,'1b95d767-358d-420d-a107-23e08306a49c','icon_image','button-rumah-jompo','button-rumah-jompo.png','image/png','public','public',20847,'[]','{\"generated_conversions\": {\"icon\": true, \"thumb\": true}}','[]',14,'2020-06-05 23:11:02','2020-06-05 23:11:02'),(16,'App\\Models\\Backend\\Kategori',4,'0ed71868-4d81-464a-8bc3-9d361aadc8a0','icon_image','button-pendidikan','button-pendidikan.png','image/png','public','public',14647,'[]','{\"generated_conversions\": {\"icon\": true, \"thumb\": true}}','[]',15,'2020-06-05 23:11:10','2020-06-05 23:11:10'),(17,'App\\Models\\Backend\\Kategori',3,'2ab96260-db3a-4203-b636-8e4afc3fc2b3','icon_image','buttonkesehatan (1)','buttonkesehatan-(1).png','image/png','public','public',13884,'[]','{\"generated_conversions\": {\"icon\": true, \"thumb\": true}}','[]',16,'2020-06-05 23:11:25','2020-06-05 23:11:25'),(18,'App\\Models\\Backend\\Kategori',2,'0f3945a5-461e-4f42-9e55-21edc7fcf6d1','icon_image','buttonpembangunan (1)','buttonpembangunan-(1).png','image/png','public','public',10901,'[]','{\"generated_conversions\": {\"icon\": true, \"thumb\": true}}','[]',17,'2020-06-05 23:11:38','2020-06-05 23:11:38'),(19,'App\\Models\\Backend\\Kategori',1,'730b99d8-6e36-4506-b2fd-26b9eca420e6','icon_image','ButtonAnakYatim','ButtonAnakYatim.png','image/png','public','public',22066,'[]','{\"generated_conversions\": {\"icon\": true, \"thumb\": true}}','[]',18,'2020-06-05 23:11:47','2020-06-05 23:11:48'),(47,'App\\Models\\Backend\\Yayasan',1,'6eda2c2d-d4f9-408a-8ef8-a1b72af0cfea','foto_image','tamman (1)','tamman-(1).jpg','image/jpeg','public','public',51613,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',45,'2020-06-06 00:24:57','2020-06-06 00:24:57'),(48,'App\\Models\\Backend\\Yayasan',2,'9bacdb5b-b029-4363-b68e-6a61f53820bb','foto_image','pondokyatim','pondokyatim.png','image/png','public','public',76320,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',46,'2020-06-06 00:26:02','2020-06-06 00:26:02'),(49,'App\\Models\\Backend\\Yayasan',3,'dfc94a09-dda6-4368-ab45-cf9a62e75054','foto_image','35266e1980ff328acdfab82fbb20ebda','35266e1980ff328acdfab82fbb20ebda.png','image/png','public','public',71282,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',47,'2020-06-06 00:26:43','2020-06-06 00:26:43'),(50,'App\\Models\\Backend\\Yayasan',4,'93e8da1e-2a1d-4b90-8183-bb43d5d978ca','foto_image','ypp','ypp.png','image/png','public','public',7964,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',48,'2020-06-06 00:27:42','2020-06-06 00:27:42'),(51,'App\\Models\\Backend\\Yayasan',5,'7bd44505-27ec-406d-b273-86c834941ea6','foto_image','db54e3ec8e7b968372a557b1dd439c40 (1)','db54e3ec8e7b968372a557b1dd439c40-(1).jpg','image/jpeg','public','public',41613,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',49,'2020-06-06 00:28:06','2020-06-06 00:28:06'),(52,'App\\Models\\Backend\\Yayasan',6,'966badbc-7cbe-4ef9-9f61-757200c33c6f','foto_image','cropped-MAULID-NABI-1 (1)','cropped-MAULID-NABI-1-(1).jpeg','image/jpeg','public','public',126013,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',50,'2020-06-06 00:29:12','2020-06-06 00:29:12'),(53,'App\\Models\\Backend\\Yayasan',7,'6d49dba2-cfd4-431e-8f53-bd754a2bc293','foto_image','9b08d1044e922b0fe46cf98fe5895efa','9b08d1044e922b0fe46cf98fe5895efa.png','image/jpeg','public','public',98051,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',51,'2020-06-06 00:29:46','2020-06-06 00:29:46'),(54,'App\\Models\\Backend\\Yayasan',8,'bd628400-a015-435d-9b34-15d94b312a00','foto_image','dorkas','dorkas.png','image/png','public','public',102451,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',52,'2020-06-06 00:30:05','2020-06-06 00:30:05'),(55,'App\\Models\\Backend\\Yayasan',9,'403396ec-bddb-4a8c-b0c4-c57431831a29','foto_image','dwituna','dwituna.png','image/png','public','public',335761,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',53,'2020-06-06 00:30:22','2020-06-06 00:30:22'),(56,'App\\Models\\Backend\\Yayasan',10,'370f3167-732d-486a-bd97-56c0d88a73e7','foto_image','835bfe068093632c3bf4c6c3e674b36f','835bfe068093632c3bf4c6c3e674b36f.jpg','image/jpeg','public','public',775366,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',54,'2020-06-06 00:31:21','2020-06-06 00:31:22'),(57,'App\\Models\\Backend\\Yayasan',11,'0cba16b5-c7a9-4062-9e00-30ddb6d1f1f2','foto_image','mitra','mitra.jpg','image/jpeg','public','public',431493,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',55,'2020-06-06 00:32:40','2020-06-06 00:32:40'),(58,'App\\Models\\Backend\\Yayasan',12,'fbe58b92-00c1-4741-9c27-94fe93b67a57','foto_image','6abcf3945c3cb76c76f3f20df0acfbaa','6abcf3945c3cb76c76f3f20df0acfbaa.png','image/png','public','public',28417,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',56,'2020-06-06 00:32:54','2020-06-06 00:32:54'),(59,'App\\Models\\Backend\\Yayasan',13,'e138b8d4-067d-4a97-b056-5be249dbf762','foto_image','a9c488544bd7d940ea1ac2d76ce5ce2e','a9c488544bd7d940ea1ac2d76ce5ce2e.png','image/png','public','public',19678,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',57,'2020-06-06 00:33:07','2020-06-06 00:33:07'),(60,'App\\Models\\Backend\\Yayasan',14,'7402d264-34c6-4e5f-9908-81e6ca0ff9e1','foto_image','6ed438ae497caf6c3b62a3dad580daeb','6ed438ae497caf6c3b62a3dad580daeb.png','image/png','public','public',409250,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',58,'2020-06-06 00:33:24','2020-06-06 00:33:25'),(61,'App\\Models\\Backend\\Yayasan',15,'72e46900-5571-4e5c-9485-2571bfb77df1','foto_image','30d51e6b2703a026d9513ae65d6bb405','30d51e6b2703a026d9513ae65d6bb405.jpg','image/jpeg','public','public',109243,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',59,'2020-06-06 00:34:33','2020-06-06 00:34:33'),(62,'App\\Models\\Backend\\Yayasan',19,'b2c5d313-05c4-4ac7-a43f-467f8710bc60','foto_image','rumahyatim','rumahyatim.jpg','image/jpeg','public','public',55022,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',60,'2020-06-06 00:36:05','2020-06-06 00:36:05'),(63,'App\\Models\\Backend\\Yayasan',18,'af5e940f-2b70-4e37-b2c9-6df567371c6b','foto_image','48c7356f1911324f561e3acc19afd461','48c7356f1911324f561e3acc19afd461.jpg','image/jpeg','public','public',93202,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',61,'2020-06-06 00:36:24','2020-06-06 00:36:24'),(64,'App\\Models\\Backend\\Yayasan',17,'d37aacd4-12a5-4901-ac61-9971a726f433','foto_image','portaldonasi','portaldonasi.jpg','image/jpeg','public','public',203401,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',62,'2020-06-06 00:36:39','2020-06-06 00:36:39'),(65,'App\\Models\\Backend\\Yayasan',16,'2d211704-4aae-4fb4-98fc-75d28c301dfe','foto_image','25f7ba1c01272c8ba933487939a94c09','25f7ba1c01272c8ba933487939a94c09.png','image/png','public','public',11590,'[]','{\"generated_conversions\": {\"thumb\": true}}','[]',63,'2020-06-06 00:38:17','2020-06-06 00:38:17');
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2020_05_22_142948_add_username_to_users_table',1),(5,'2020_05_22_155457_create_permission_tables',1),(6,'2020_05_22_155531_add_soft_to_permissions_table',1),(7,'2020_05_22_223522_create_media_table',1),(8,'2020_05_23_193459_create_notifications_table',1),(9,'2020_06_01_155919_create_kategori_table',1),(10,'2020_06_01_160350_create_provinces_table',1),(11,'2020_06_01_160408_create_yayasan_table',1),(12,'2020_06_01_162155_add_stamps_to_yayasan_table',1),(13,'2020_06_01_181136_create_konfirmasi_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` VALUES (1,'App\\User',1),(2,'App\\User',2),(4,'App\\User',3),(5,'App\\User',4),(4,'App\\User',5),(5,'App\\User',6);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) unsigned NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'access_authorization','web','2020-06-03 22:25:40','2020-06-03 22:25:40',NULL),(2,'super_admin','web','2020-06-03 22:25:40','2020-06-03 22:25:40',NULL),(3,'update_role','web','2020-06-03 22:25:40','2020-06-03 22:25:40',NULL),(4,'delete_role','web','2020-06-03 22:25:40','2020-06-03 22:25:40',NULL),(5,'create_role','web','2020-06-03 22:25:40','2020-06-03 22:25:40',NULL),(6,'list_role','web','2020-06-03 22:25:40','2020-06-03 22:25:40',NULL),(7,'show_role','web','2020-06-03 22:25:40','2020-06-03 22:25:40',NULL),(8,'update_permission','web','2020-06-03 22:25:40','2020-06-03 22:25:40',NULL),(9,'delete_permission','web','2020-06-03 22:25:40','2020-06-03 22:25:40',NULL),(10,'create_permission','web','2020-06-03 22:25:40','2020-06-03 22:25:40',NULL),(11,'list_permission','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(12,'show_permission','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(13,'update_user','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(14,'delete_user','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(15,'create_user','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(16,'list_user','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(17,'show_user','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(18,'update_kategori','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(19,'delete_kategori','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(20,'create_kategori','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(21,'list_kategori','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(22,'show_kategori','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(23,'update_provinsi','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(24,'delete_provinsi','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(25,'create_provinsi','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(26,'list_provinsi','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(27,'show_provinsi','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(28,'update_yayasan','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(29,'approve_yayasan','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(30,'delete_yayasan','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(31,'create_yayasan','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(32,'list_yayasan','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(33,'show_yayasan','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provinces`
--

DROP TABLE IF EXISTS `provinces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provinces` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provinces`
--

LOCK TABLES `provinces` WRITE;
/*!40000 ALTER TABLE `provinces` DISABLE KEYS */;
INSERT INTO `provinces` VALUES (11,'Aceh',NULL,'2020-06-04 18:38:30',NULL,'aceh'),(12,'Sumatera Utara',NULL,'2020-06-04 18:38:30',NULL,'sumatera-utara'),(13,'Sumatera Barat',NULL,'2020-06-04 18:38:30',NULL,'sumatera-barat'),(14,'Riau',NULL,'2020-06-04 18:38:30',NULL,'riau'),(15,'Jambi',NULL,'2020-06-04 18:38:30',NULL,'jambi'),(16,'Sumatera Selatan',NULL,'2020-06-04 18:38:30',NULL,'sumatera-selatan'),(17,'Bengkulu',NULL,'2020-06-04 18:38:30',NULL,'bengkulu'),(18,'Lampung',NULL,'2020-06-04 18:38:30',NULL,'lampung'),(19,'Kepulauan Bangka Belitung',NULL,'2020-06-04 18:38:30',NULL,'kepulauan-bangka-belitung'),(21,'Kepulauan Riau',NULL,'2020-06-04 18:38:30',NULL,'kepulauan-riau'),(31,'Dki Jakarta',NULL,'2020-06-04 18:38:30',NULL,'dki-jakarta'),(32,'Jawa Barat',NULL,'2020-06-04 18:38:30',NULL,'jawa-barat'),(33,'Jawa Tengah',NULL,'2020-06-04 18:38:30',NULL,'jawa-tengah'),(34,'Di Yogyakarta',NULL,'2020-06-04 18:38:30',NULL,'di-yogyakarta'),(35,'Jawa Timur',NULL,'2020-06-04 18:38:30',NULL,'jawa-timur'),(36,'Banten',NULL,'2020-06-04 18:38:30',NULL,'banten'),(51,'Bali',NULL,'2020-06-04 18:38:30',NULL,'bali'),(52,'Nusa Tenggara Barat',NULL,'2020-06-04 18:38:30',NULL,'nusa-tenggara-barat'),(53,'Nusa Tenggara Timur',NULL,'2020-06-04 18:38:30',NULL,'nusa-tenggara-timur'),(61,'Kalimantan Barat',NULL,'2020-06-04 18:38:30',NULL,'kalimantan-barat'),(62,'Kalimantan Tengah',NULL,'2020-06-04 18:38:30',NULL,'kalimantan-tengah'),(63,'Kalimantan Selatan',NULL,'2020-06-04 18:38:30',NULL,'kalimantan-selatan'),(64,'Kalimantan Timur',NULL,'2020-06-04 18:38:30',NULL,'kalimantan-timur'),(65,'Kalimantan Utara',NULL,'2020-06-04 18:38:30',NULL,'kalimantan-utara'),(71,'Sulawesi Utara',NULL,'2020-06-04 18:38:30',NULL,'sulawesi-utara'),(72,'Sulawesi Tengah',NULL,'2020-06-04 18:38:30',NULL,'sulawesi-tengah'),(73,'Sulawesi Selatan',NULL,'2020-06-04 18:38:30',NULL,'sulawesi-selatan'),(74,'Sulawesi Tenggara',NULL,'2020-06-04 18:38:30',NULL,'sulawesi-tenggara'),(75,'Gorontalo',NULL,'2020-06-04 18:38:30',NULL,'gorontalo'),(76,'Sulawesi Barat',NULL,'2020-06-04 18:38:30',NULL,'sulawesi-barat'),(81,'Maluku',NULL,'2020-06-04 18:38:30',NULL,'maluku'),(82,'Maluku Utara',NULL,'2020-06-04 18:38:30',NULL,'maluku-utara'),(91,'Papua Barat',NULL,'2020-06-04 18:38:30',NULL,'papua-barat'),(94,'Papua',NULL,'2020-06-04 18:38:30',NULL,'papua');
/*!40000 ALTER TABLE `provinces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(1,2),(3,2),(4,2),(5,2),(6,2),(8,2),(9,2),(10,2),(11,2),(13,2),(14,2),(15,2),(16,2),(1,3),(3,3),(4,3),(5,3),(6,3),(13,3),(14,3),(15,3),(16,3),(1,4),(28,4),(30,4),(31,4),(32,4),(33,4),(1,5);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'superadmin','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(2,'admin','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(3,'staff','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(4,'yayasan','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL),(5,'donatur','web','2020-06-03 22:25:41','2020-06-03 22:25:41',NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` enum('pending','active','nonactive','reject') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Iman Sugirman','nakamura.agatha@gmail.com',NULL,NULL,'$2y$10$JMdesCPCr0.3HGjhrxwacuCkurLbKx60vEozU9dHKBDbVmovGwfj6',NULL,'2020-06-03 22:25:40',NULL,'active',NULL),(2,'Admin','admin@gmail.com',NULL,NULL,'$2y$10$ruNv2XCSARDQSWQCKdSzNuQROFHPPTKr8VbFQpX9bl9aA6ATHvUXG',NULL,'2020-06-03 22:25:40',NULL,'active',NULL),(3,'Member','member@gmail.com',NULL,NULL,'$2y$10$IaMOrhSLHoohCQB1cp3SuuXRg9FGKsHm33xhSIsXsPf0bS5VvsLo2',NULL,'2020-06-03 22:25:40',NULL,'active',NULL),(4,'Cekers','bogordesainads@gmail.com',NULL,NULL,'$2y$10$/yQYjffiwnWQvg8xTlFkrOcH.bm837lG1bpN8z2zIpbfDtT5CHZ9S',NULL,'2020-06-05 00:17:37','2020-06-05 00:17:37','pending',NULL),(5,'c','gkfxprime.indonesia@gmail.com',NULL,NULL,'$2y$10$Kb1WUfFJpUs/6zQte.zZyOVFjfQyph38N1flBqw8j/pGFVvZKjc3C',NULL,'2020-06-06 00:41:14','2020-06-06 00:41:14','pending',NULL),(6,'f','junomarkets.video@gmail.com',NULL,NULL,'$2y$10$IFffbgGQdBKA6O8Q4HfmouuT.vPtG3tlkwei2vwiacUtgEYJoWgrK',NULL,'2020-06-06 00:42:03','2020-06-06 00:42:03','pending',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yayasan`
--

DROP TABLE IF EXISTS `yayasan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yayasan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kategori_id` int(10) unsigned NOT NULL,
  `provinces_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pemilik` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_izin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_rekening` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_rekening` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_bank` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_yayasan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('publish','pending','reject') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) unsigned DEFAULT NULL,
  `updated_by` bigint(20) unsigned DEFAULT NULL,
  `deleted_by` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yayasan`
--

LOCK TABLES `yayasan` WRITE;
/*!40000 ALTER TABLE `yayasan` DISABLE KEYS */;
INSERT INTO `yayasan` VALUES (1,4,36,1,'Yayasan Taman Mandiri Syariah','yayasan-taman-mandiri-syariah','Yayasan Taman Mandiri Syariah','0000000','Yayasan Taman Mandiri Syariah','6800740761','BCA','peduliyts@gmail.com','087870840415','Jln.Raya Pondok Aren Ruko Arinda Permai 1 Rt.04/Rw.04 Blok A1 No.11 Kel.Pondok Aren Kec.Pondok Aren Tangerang Selatan 15224','/tmp/phpnQMy2k','publish','2020-06-03 22:29:52','2020-06-06 00:24:57',NULL,1,1,NULL),(2,1,31,1,'Pondok Yatim','pondok-yatim','Pondok Yatim','0000000','Yayasan Amal Sholeh Sejahtera','3091277474','BCA','info@pondokyatim.or.id','0812 2343 7474','Jalan Kedoya Raya No. 2A, RT. 07 RW. 03, Kedoya Selatan, Kebon Jeruk, Jakarta Barat, Indonesia 11520','/tmp/phpRnsxLX','publish','2020-06-03 22:32:32','2020-06-06 00:26:02',NULL,1,1,NULL),(3,4,11,1,'Perhimpunan Vincentius Jakarta','perhimpunan-vincentius-jakarta','Perhimpunan Vincentius Jakarta','0000000','Perhimpunan Vincentius Jakarta','6340045350','BCA','info@vincentius.or.id','081218900314','Jl. Kramat Raya No. 134 Jakarta Pusat 10430. Indonesia','/tmp/phpWni0oB','publish','2020-06-04 18:46:02','2020-06-06 00:26:43',NULL,1,1,NULL),(4,6,31,1,'Yayasan Pundi Amal Peduli Kasih','yayasan-pundi-amal-peduli-kasih','Yayasan Pundi Amal Peduli Kasih','0000000','Yayasan Pundi Amal Peduli Kasih','5005772000','BCA','sekretariat.ypapk@gmail.com','02127935555/5672222','SCTV Tower – Senayan City Jl. Asia Afrika Lot. 19 Jakarta 10270 Jakarta Pusat','/tmp/phpcXqh2W','publish','2020-06-05 17:05:35','2020-06-06 00:27:42',NULL,1,1,NULL),(5,6,31,1,'Yayasan Sayap Ibu Jakarta','yayasan-sayap-ibu-jakarta','Yayasan Sayap Ibu Jakarta','0000000','Yayasan Sayap Ibu Jakarta','2283003019','BCA','admin@sayapibujakarta.org','0217221763','Jl. Barito II No.55 Kebayoran Baru Jakarta Selatan 12130','/tmp/phpZPLYjY','publish','2020-06-05 17:08:22','2020-06-06 00:28:06',NULL,1,1,NULL),(6,4,36,1,'Yayasan Global Mulia Insani','yayasan-global-mulia-insani','Yayasan Global Mulia Insani','0000000','Yayasan Global Mulia Insani','1640002690040','Mandiri','ygmipeduli@gmail.com','085779569608','Jl. Sumatra Blok A No. 11 RT.007/RW. 022 Kel.Jombang Kec. Ciputat Kota Tangerang Selatan','/tmp/php5sUrOD','publish','2020-06-05 17:11:16','2020-06-06 00:29:12',NULL,1,1,NULL),(7,4,32,1,'Mandiri Amanah','mandiri-amanah','Mandiri Amanah','0000000','Yayasan Mandiri','1330014806806','Mandiri','info@mandiriamanah.org','021 8945 2133','Jl. Raya AMD Tegallega No. 19 Rt. 001 Rw. 002 Ds. Sinarjaya Kec. Sukamakmur Kab. Bogor Jawa Barat','/tmp/phpQiOAZH','publish','2020-06-05 23:34:51','2020-06-06 00:29:46',NULL,1,1,NULL),(8,4,31,1,'Dorkas Orphanage','dorkas-orphanage','Dorkas Orphanage','0000000','Perkumpulan Dorkas (Panti Asuhan)','0753022705','BCA','panti.dorkas@yahoo.com','081211421140','Jl. Kramat Sentiong No.20-22, RT.5/RW.7, Kramat, Kec. Senen, Kota Jakarta Pusat','/tmp/phpDA7FmM','publish','2020-06-05 23:37:18','2020-06-06 00:30:05',NULL,1,1,NULL),(9,4,31,1,'Yayasan Pendidikan Dwituna Rawinala','yayasan-pendidikan-dwituna-rawinala','Yayasan Pendidikan Dwituna Rawinala','0000000','Yayasan Pendidikan Dwituna Rawinala','0943002689','BCA','contact@rawinala.org','+62218090407','Jalan Inerbang 38, Kramat Jati DKI Jakarta 13520 Indonesia','/tmp/phpFQtE5L','publish','2020-06-05 23:40:33','2020-06-06 00:30:22',NULL,1,1,NULL),(10,4,31,1,'Yayasan Amal Mulia Indonesia','yayasan-amal-mulia-indonesia','Yayasan Amal Mulia Indonesia','0000000','Yayasan Amal Mulia Indonesia','0255203103','BNI','info@amalmulia.id','087887768388','Jl. Masjid Al Mubarok No.16 RT/RW.006/010 Cipulir Kebayoran Lama Jakarta Selatan 12230','/tmp/phpPeRDER','publish','2020-06-05 23:42:21','2020-06-06 00:31:22',NULL,1,1,NULL),(11,4,35,1,'Yayasan Mitra Arofah','yayasan-mitra-arofah','Yayasan Mitra Arofah','0000000','Yayasan Mitra Arafah','0054545850','BNI','mitraarofah@gmail.com','08123226294','Jl wonocolo 8/32, Jemur Wonosari, Surabaya, Kota SBY, Jawa Timur 60237','/tmp/php3jxpf2','publish','2020-06-05 23:45:39','2020-06-06 00:32:40',NULL,1,1,NULL),(12,3,31,1,'Yayasan Kesehatan Payudara Jakarta','yayasan-kesehatan-payudara-jakarta','Yayasan Kesehatan Payudara Jakarta','0000000','Yayasan Kesehatan Payudara Jakarta','1260007059198','Mandiri','ykpi.sekretariat@gmail.com','0217202484','Grand Wijaya Center Blok H9 Jl. Wijaya 2 Kebayoran Jakarta Selatan','/tmp/php2bTrO3','publish','2020-06-05 23:47:34','2020-06-06 00:32:54',NULL,1,1,NULL),(13,3,31,1,'Yayasan Kanker Indonesia','yayasan-kanker-indonesia','Yayasan Kanker Indonesia','0000000','Yayasan Kanker Indonesia','2069227788','BCA','admin@teledonasiyki.org','081292194244','YKI Pusat Jalan Sam Ratulangi no 35, Menteng, Jakarta','/tmp/phpjaSVQW','publish','2020-06-05 23:48:48','2020-06-06 00:33:07',NULL,1,1,NULL),(14,1,32,1,'Panti Yauma','panti-yauma','Panti Yauma','0000000','Yayasan Umat Mandiri','4180600006','BCA','adm@panti-yauma.com','085320000911','Jl. Kh. Abdul Halim No.21, Cijati, Kec. Majalengka, Kabupaten Majalengka, Jawa Barat 45417','/tmp/phpcNe3Rd','publish','2020-06-05 23:51:04','2020-06-06 00:33:25',NULL,1,1,NULL),(15,1,31,1,'Yayasan Yatim Piatu Rasulullah SAW','yayasan-yatim-piatu-rasulullah-saw','Yayasan Yatim Piatu Rasulullah SAW','0000000','Yayasan Yatim Piatu Rasulullah SAW','6530355797','BCA','info@yatimrasulullah.or.id','08129289574','Jl. Petojo Binatu Raya No.19, RT.13/RW.8, Petojo Utara, Kecamatan Gambir, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10130','/tmp/phpQvcg0A','publish','2020-06-05 23:52:45','2020-06-06 00:34:33',NULL,1,1,NULL),(16,1,31,1,'Yayasan Rumah Piatu Muslimin','yayasan-rumah-piatu-muslimin','Yayasan Rumah Piatu Muslimin','0000000','Yayasan Rumah Piatu Muslimin','6090355552','BCA','info@ver02.rumahpiatu.org','021 3107901','Jalan Kramat Raya No. 11 Jakarta Pusat 10450','/tmp/phpssFdEE','publish','2020-06-05 23:54:11','2020-06-06 00:38:17',NULL,1,1,NULL),(17,1,32,1,'Portal Donasi','portal-donasi','Portal Donasi','0000000','Yayasan Portal Donasi','1730017817827','Mandiri','info@portaldonasi.com','085722222688','Ruko Race Resinda TA7-12, Telukjambe Timur Karawang, Jawa Barat. 41361','/tmp/phpoDzdE3','publish','2020-06-05 23:55:54','2020-06-06 00:36:39',NULL,1,1,NULL),(18,1,36,1,'Yayasan Kota Maju Gemilang','yayasan-kota-maju-gemilang','Yayasan Kota Maju Gemilang','0000000','Yayasan Kota Maju Gemilang','0394904755','BNI','ykmgtangerang@gmail.com','081385352154','Jl. DR Setia Budi No.198 Rt.001/009 Kebon Manggis, Parung serab, Ciledug Kota Tangerang','/tmp/phpm9I2sR','publish','2020-06-05 23:58:06','2020-06-06 00:36:24',NULL,1,1,NULL),(19,1,32,1,'Rumah Yatim','rumah-yatim','Rumah Yatim','0000000','Rumah Yatim','033701000930309','BRI','info@rumah-yatim.org','081221200900','Jl. Buah Batu No.296, Kel. Cijagra, Kec. Lengkong, Kota Bandung, Jawa Barat 40265','/tmp/phpG1noAj','publish','2020-06-05 23:59:32','2020-06-06 00:36:05',NULL,1,1,NULL);
/*!40000 ALTER TABLE `yayasan` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-07  9:41:08
