<?php

return [
    'credentials_path'	=>  public_path('vision.json'),
    'api_key'		=>  env('VISION_API_KEY')
];