<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\YayasanStatus;

class CreateYayasanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yayasan', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('kategori_id')->references('id')->on('kategori');
            $table->unsignedInteger('provinces_id')->references('id')->on('provinces');
            $table->unsignedInteger('user_id')->references('id')->on('users');
            $table->string('name');
            $table->string('slug');
            $table->string('pemilik');
            $table->string('no_izin');
            $table->string('nama_rekening');
            $table->string('no_rekening');
            $table->string('nama_bank');
            $table->string('email_yayasan');
            $table->string('phone');
            $table->string('alamat');
            $table->string('foto')->nullable();
            $table->enum('status', ['Publish' => YayasanStatus::Publish, 'Pending' => YayasanStatus::Pending, 'Reject' => YayasanStatus::Reject])->default(YayasanStatus::Pending);
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yayasan');
    }
}
