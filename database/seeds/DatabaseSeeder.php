<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        // $this->call(WilayahTableSeeder::class);
        // $this->call(KategoriTableSeeder::class);
        $this->call(UserRolesTableSeeder::class);
        $this->call(ProvinsiTableSeeder::class);
    }
}
