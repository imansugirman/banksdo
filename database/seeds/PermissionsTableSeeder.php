<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'access_authorization']);
        Permission::create(['name' => 'super_admin']);

		Permission::create(['name' => 'update_role']);
		Permission::create(['name' => 'delete_role']);
		Permission::create(['name' => 'create_role']);
		Permission::create(['name' => 'list_role']);
		Permission::create(['name' => 'show_role']);

		Permission::create(['name' => 'update_permission']);
		Permission::create(['name' => 'delete_permission']);
		Permission::create(['name' => 'create_permission']);
		Permission::create(['name' => 'list_permission']);
		Permission::create(['name' => 'show_permission']);

		Permission::create(['name' => 'update_user']);
		Permission::create(['name' => 'delete_user']);
		Permission::create(['name' => 'create_user']);
		Permission::create(['name' => 'list_user']);
		Permission::create(['name' => 'show_user']);

		Permission::create(['name' => 'update_kategori']);
		Permission::create(['name' => 'delete_kategori']);
		Permission::create(['name' => 'create_kategori']);
		Permission::create(['name' => 'list_kategori']);
		Permission::create(['name' => 'show_kategori']);

		Permission::create(['name' => 'update_provinsi']);
		Permission::create(['name' => 'delete_provinsi']);
		Permission::create(['name' => 'create_provinsi']);
		Permission::create(['name' => 'list_provinsi']);
		Permission::create(['name' => 'show_provinsi']);

		Permission::create(['name' => 'update_yayasan']);
		Permission::create(['name' => 'approve_yayasan']);
		Permission::create(['name' => 'delete_yayasan']);
		Permission::create(['name' => 'create_yayasan']);
		Permission::create(['name' => 'list_yayasan']);
		Permission::create(['name' => 'show_yayasan']);
    }

}
