<?php

use Flynsarmy\CsvSeeder\CsvSeeder;
use Illuminate\Database\Seeder;

class ProvinsiTableSeeder extends CsvSeeder
{
    public function __construct()
	{
		$this->table = 'provinces';
        $this->csv_delimiter = ';';
        $this->filename = base_path().'/database/seeds/csv/provinsi.csv';
        $this->offset_rows = 1;
        $this->mapping = [
            0 => 'id',
            1 => 'name',
        ];
        $this->should_trim = true;
	}

	public function run()
	{
		// Recommended when importing larger CSVs
		DB::disableQueryLog();

		// Uncomment the below to wipe the table clean before populating
		DB::table($this->table)->truncate();

		parent::run();
	}
}
