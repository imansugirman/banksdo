<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $role = Role::create(['name' => 'superadmin']);
        $role->givePermissionTo(Permission::all());

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo(
            [
                'access_authorization',

                'update_user',
                'delete_user',
                'create_user',
                'list_user',

                'update_permission',
                'delete_permission',
                'create_permission',
                'list_permission',

                'update_role',
                'delete_role',
                'create_role',
                'list_role',
                
            ]
        );

        $role = Role::create(['name' => 'staff']);
            $role->givePermissionTo(
            [
                'access_authorization',

                'update_user',
                'delete_user',
                'create_user',
                'list_user',

                'update_role',
                'delete_role',
                'create_role',
                'list_role',
            ]
        );

        $role = Role::create(['name' => 'yayasan']);
            $role->givePermissionTo(
            [
                'access_authorization',

                'update_yayasan',
                'delete_yayasan',
                'create_yayasan',
                'list_yayasan',
                'show_yayasan',
            ]
        );

        $role = Role::create(['name' => 'donatur']);
            $role->givePermissionTo(
            [
                'access_authorization',
            ]
        );
		 
    }

}
