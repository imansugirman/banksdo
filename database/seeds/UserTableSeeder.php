<?php

use App\Enums\UserStatus;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Iman Sugirman',
            'email' => 'nakamura.agatha@gmail.com',
            'status' => UserStatus::Active,
            'created_at' => Carbon::now(),
            'password' => bcrypt('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'status' => UserStatus::Active,
            'created_at' => Carbon::now(),
            'password' => bcrypt('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'Member',
            'email' => 'member@gmail.com',
            'status' => UserStatus::Active,
            'created_at' => Carbon::now(),
            'password' => bcrypt('password'),
        ]);
    }
}
