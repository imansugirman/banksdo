<?php

use App\Enums\UserStatus;
use App\Enums\YayasanStatus;


return [
    UserStatus::class => [
        UserStatus::Pending => 'Pending',
        UserStatus::Active => 'Active',
        UserStatus::NonActive => 'Non Active',
        UserStatus::Reject => 'Reject',
    ],

    YayasanStatus::class => [
        YayasanStatus::Pending => 'Pending',
        YayasanStatus::Publish => 'Publish',
        YayasanStatus::Reject => 'Reject',
    ],
];
