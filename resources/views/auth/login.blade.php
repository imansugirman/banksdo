<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bank Donation</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ mix('themes/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/ionicons.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/themes.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/skins.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/icheck.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">


</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="{{ url('/home') }}"><img src="{{ asset('themes/img/logobankdonation.png')}}"></a>
    </div>

    <div class="login-box-body">
        <p class="login-box-msg">Login</p>
        @include('flash::message')

        {{ Aire::open()->post()->action(route ('login')) }}
            {{ Aire::email('email')
                ->placeholder('Email')
                ->required() }}
            {{ Aire::password('password')
                ->placeholder('Password')
                ->required() }}
            {{ Aire::submit('Sign In')->addClass('btn-block') }}
        {{ Aire::close() }}
       {{--  <form method="post" action="{{ url('/login') }}">
            @csrf

            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" placeholder="Password" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif

            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        </form> --}}
        <hr>
        <div class="row">
            <div class="col-md-6">
                <a href="{{ url('/password/reset') }}" class="btn btn-info btn-sm btn-block">Lupa Password?</a>
            </div>
            <div class="col-md-6">
                <a href="{{ url('/daftar') }}" class="btn btn-success btn-sm btn-block">Daftar</a>
            </div>
        </div>

        
        

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<script src="{{ mix('themes/js/jquery.min.js')}}"></script>
<script src="{{ mix('themes/js/bootstrap.min.js')}}"></script>
<script src="{{ mix('themes/js/themes.min.js')}}"></script>
<script src="{{ mix('themes/js/icheck.min.js')}}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>
