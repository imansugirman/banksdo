<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Polisi Apps</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ mix('themes/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/ionicons.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/themes.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/skins.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/icheck.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="{{ url('/home') }}"><b>Polisi Apps </b></a>
    </div>

    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Enter Email to reset password</p>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        {{ Aire::open()->post()->action(route ('password.email'))}}
            {{ Aire::email('email')->placeholder('Email') }}
            {{ Aire::submit('Reset Password')->addClass('btn-success btn-block') }}
        {{ Aire::close() }}

        {{-- <form method="post" action="{{ url('/password/email') }}">
            @csrf

            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>

            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btn-block">
                        <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
                    </button>
                </div>
            </div>

        </form> --}}

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<script src="{{ mix('themes/js/jquery.min.js')}}"></script>
<script src="{{ mix('themes/js/bootstrap.min.js')}}"></script>
<script src="{{ mix('themes/js/themes.min.js')}}"></script>
<script src="{{ mix('themes/js/icheck.min.js')}}"></script>
</body>
</html>
