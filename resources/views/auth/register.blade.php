<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bank Donation</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ mix('themes/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/select2.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/ionicons.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/themes.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/skins.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/icheck.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/custom.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
</head>
<body class="hold-transition register-page">
<div class="register-box">
    <div class="register-logo">
        <a href="{{ url('/home') }}"><img src="{{ asset('themes/img/logobankdonation.png')}}"></a>
    </div>

    <div class="register-box-body">
        <p class="login-box-msg">Daftar Menjadi Akun Anda</p>
<!--         <div class="bs-callout bs-callout-warning hidden">
          <h4>Oh snap!</h4>
          <p>This form seems to be invalid :(</p>
        </div> -->
        
        {!! Form::open(['route' => 'daftarsukses', 'method' => 'post']) !!}
            <div class="form-group">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nama Lengkap']) !!}
            </div>
            <div class="form-group">
                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
            </div>
            <div class="form-group">
                {!! Form::password('password', ['class' => 'form-control', 'label' => false, 'placeholder' => 'Password']) !!}
            </div>  
            <div class="form-group">
                {!! Form::password('password_confirmation', ['class' => 'form-control', 'label' => false, 'placeholder' => 'Password Confirmation']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('roles', 'Daftar Sebagai :') !!}
                {!! Form::select('roles', $roles, null,  ['class' => 'form-control', 'id' => 'select_roles']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Daftar Sekarang', ['class' => 'btn btn-primary btn-block']) !!}
            </div>            
        {!! Form::close() !!}
        
        
        
        <hr>
    </div>
</div>

<script src="{{ mix('themes/js/jquery.min.js')}}"></script>
<script src="{{ mix('themes/js/bootstrap.min.js')}}"></script>
<script src="{{ mix('themes/js/themes.min.js')}}"></script>
<script src="{{ mix('themes/js/icheck.min.js')}}"></script>
<script src="{{ mix('themes/js/select2.min.js')}}"></script>
<script src="{{ mix('themes/js/parsley.min.js')}}"></script>


<script>
    
    $('#select_roles').select2()
</script>
</body>
</html>
