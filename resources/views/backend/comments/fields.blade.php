<!-- Commentable Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('commentable_type', 'Commentable Type:') !!}
    {!! Form::text('commentable_type', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Commentable Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('commentable_id', 'Commentable Id:') !!}
    {!! Form::number('commentable_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Comment Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('comment', 'Comment:') !!}
    {!! Form::textarea('comment', null, ['class' => 'form-control']) !!}
</div>

<!-- Ip Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ip_address', 'Ip Address:') !!}
    {!! Form::text('ip_address', null, ['class' => 'form-control','maxlength' => 45,'maxlength' => 45]) !!}
</div>

<!-- User Agent Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_agent', 'User Agent:') !!}
    {!! Form::text('user_agent', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('backend.comments.index') }}" class="btn btn-default">Cancel</a>
</div>
