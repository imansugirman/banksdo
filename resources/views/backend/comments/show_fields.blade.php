<!-- Commentable Type Field -->
<div class="form-group">
    {!! Form::label('commentable_type', 'Commentable Type:') !!}
    <p>{{ $comments->commentable_type }}</p>
</div>

<!-- Commentable Id Field -->
<div class="form-group">
    {!! Form::label('commentable_id', 'Commentable Id:') !!}
    <p>{{ $comments->commentable_id }}</p>
</div>

<!-- Comment Field -->
<div class="form-group">
    {!! Form::label('comment', 'Comment:') !!}
    <p>{{ $comments->comment }}</p>
</div>

<!-- Ip Address Field -->
<div class="form-group">
    {!! Form::label('ip_address', 'Ip Address:') !!}
    <p>{{ $comments->ip_address }}</p>
</div>

<!-- User Agent Field -->
<div class="form-group">
    {!! Form::label('user_agent', 'User Agent:') !!}
    <p>{{ $comments->user_agent }}</p>
</div>

