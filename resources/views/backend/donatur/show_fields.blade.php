<!-- Nama Lengkap Field -->
<div class="form-group">
    {!! Form::label('nama_lengkap', 'Nama Lengkap:') !!}
    <p>{{ $donatur->nama_lengkap }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $donatur->email }}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{{ $donatur->phone }}</p>
</div>

<!-- Asuransi Field -->
<div class="form-group">
    {!! Form::label('asuransi', 'Asuransi:') !!}
    <p>{{ $donatur->asuransi }}</p>
</div>

