{!! Form::open(['route' => ['backend.kategori.destroy', $id], 'method' => 'delete']) !!}
    <a href="{{ route('backend.kategori.show', $id) }}" class='btn btn-success btn-xs'>
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{{ route('backend.kategori.edit', $id) }}" class='btn btn-info btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => "return confirm('Are you sure?')"
    ]) !!}
{!! Form::close() !!}
