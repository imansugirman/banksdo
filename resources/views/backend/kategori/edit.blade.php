@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Kategori
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            {!! Form::model($kategori, ['route' => ['backend.kategori.update', $kategori->id], 'method' => 'patch', 'files' => true, 'enctype' => 'multipart/form-data']) !!}
                                @include('backend.kategori.fields')
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
@endsection