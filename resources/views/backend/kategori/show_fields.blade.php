

<img src="{{ $kategori->getFirstMediaUrl('banner_image') }}" width="530">
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $kategori->name }}</p>
</div>

<!-- Banner Field -->
<div class="form-group">
    {!! Form::label('banner', 'Banner:') !!}
    <p></p>
</div>

<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $kategori->description }}</p>
</div>

<div class="form-group">
    {!! Form::label('icon', 'Icon:') !!}
    <p><img src="{{ $kategori->getFirstMediaUrl('icon_image') }}" width="100">
</p>
</div>

<!-- Color Field -->
<div class="form-group">
    {!! Form::label('color', 'Color:') !!}
    <p>{{ $kategori->color }}</p>
</div>

