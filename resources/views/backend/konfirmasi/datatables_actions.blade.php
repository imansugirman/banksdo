{!! Form::open(['route' => ['backend.konfirmasi.destroy', $id], 'method' => 'delete']) !!}
    <a href="{{ route('backend.konfirmasi.show', $id) }}" class='btn btn-success btn-xs'>
        <i class="glyphicon glyphicon-eye-open"></i> Detail
    </a>
    {{-- <a href="{{ route('backend.konfirmasi.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a> --}}
    {{-- {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => "return confirm('Are you sure?')"
    ]) !!} --}}
{!! Form::close() !!}
