@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Konfirmasi
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($konfirmasi, ['route' => ['backend.konfirmasi.update', $konfirmasi->id], 'method' => 'patch']) !!}

                        @include('backend.konfirmasi.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection