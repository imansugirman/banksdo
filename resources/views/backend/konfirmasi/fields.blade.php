<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Nominal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nominal', 'Nominal:') !!}
    {!! Form::text('nominal', null, ['class' => 'form-control']) !!}
</div>

<!-- Tgl Transfer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tgl_transfer', 'Tgl Transfer:') !!}
    {!! Form::text('tgl_transfer', null, ['class' => 'form-control','id'=>'tgl_transfer']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#tgl_transfer').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('backend.konfirmasi.index') }}" class="btn btn-default">Cancel</a>
</div>
