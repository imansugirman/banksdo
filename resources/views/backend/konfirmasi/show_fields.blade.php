<table class="table table-responsive table-bordered">
    <thead>
        <tr>
            <th>Field</th>
            <th>Data</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{!! Form::label('name', 'Nama Donatur') !!}</td>
            <td>{{ $konfirmasi->name }}</td>
        </tr>
        <tr>
            <td>{!! Form::label('email', 'Email Donatur') !!}</td>
            <td>{{ $konfirmasi->email }}</td>
        </tr>
        <tr>
            <td>{!! Form::label('nominal', 'Nominal') !!}</td>
            <td>{{ $konfirmasi->nominal }}</td>
        </tr>
        <tr>
            <td>{!! Form::label('tgl_transfer', 'Tgl Transfer') !!}</td>
            <td>{{ $konfirmasi->tgl_transfer->format('d M Y') }}</td>
        </tr>
        <tr>
            <td>{!! Form::label('created_at', 'Tgl Konfirmasi') !!}</td>
            <td>{{ $konfirmasi->created_at->format('d M Y H:s') }}</td>
        </tr>
    </tbody>
</table>
