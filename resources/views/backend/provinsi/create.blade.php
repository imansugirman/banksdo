@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Provinsi
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'backend.provinsi.store']) !!}

                        @include('backend.provinsi.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
