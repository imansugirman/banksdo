@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Role</h1>
    </section>
    <div class="content">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @include('adminlte-templates::common.errors')
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            {!! Form::open(['route' => 'backend.roles.store']) !!}
                                @include('backend.roles.fields')
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>   
        </div>      
    </div>
@endsection
