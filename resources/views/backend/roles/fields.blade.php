<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Guard Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('guard_name', 'Guard Name:') !!}
    {!! Form::text('guard_name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    @foreach ($permissions as $permission)
    	<div class="col-md-4">	    	
	    	<div class="form-check form-check-inline">
	    		{{ Form::checkbox('permissions[]',  $permission->id, null, ['class' => 'name'] ) }}
                	{{ Form::label($permission->name) }}

	    	</div>
    	</div>
    @endforeach
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('backend.roles.index') }}" class="btn btn-default">Cancel</a>
</div>



@push('scripts')
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-orange',
            radioClass: 'iradio_square-orange',
            increaseArea: '20%' // optional
        });
    });
</script>
@endpush
