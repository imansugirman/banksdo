@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Role</h1>
    </section>
    <div class="content">
        <div class="row" style="padding-left: 20px">
            @include('backend.roles.show_fields')
        </div>
    </div>
@endsection
