<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-body">
            <div class="box-tools pull-left">
                <div class="box-title"><h4>Role Name</h4></div>
            </div>
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <td>{!! Form::label('name', 'Name:') !!}</td>
                        <td>{{ $role->name }}</td>
                    </tr>
                    <tr>
                        <td>{!! Form::label('guard_name', 'Guard Name:') !!}</td>
                        <td>{{ $role->guard_name }}</td>
                    </tr>
                </tbody>
            </table>		    		    
        </div>
    </div>
    <a href="{{ route('backend.roles.index') }}" class="btn btn-default">Back</a>  
</div>


<div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                <div class="box-tools pull-left">
                    <div class="box-title"><h4>Permission</h4></div>
                </div>
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td class="row">
                                @foreach ($rolePermission as $item)
                                    <div class="col-md-6">
                                        <i class="fa fa-key" aria-hidden="true"></i> {{ $item->name }}
                                    </div>
                                @endforeach
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
