@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Yayasan</h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            {!! Form::open(['route' => 'backend.yayasan.store', 'files' => true, 'enctype' => 'multipart/form-data']) !!}
                                {!! Form::hidden('user_id', Auth::user()->id) !!}
                                
                                @include('backend.yayasan.fields')
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
