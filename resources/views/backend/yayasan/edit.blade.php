@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Yayasan
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            {!! Form::model($yayasan, ['route' => ['backend.yayasan.update', $yayasan->id], 'method' => 'patch', 'files' => true, 'enctype' => 'multipart/form-data']) !!}
                                {!! Form::hidden('user_id', $creator) !!}
                                <img src={{ $yayasan->getFirstMediaUrl('foto_image') }} width="350" style="margin-bottom:30px;padding: 20px">

                                @include('backend.yayasan.fields')
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
@endsection