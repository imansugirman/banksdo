<div class="form-group col-sm-12">
    {!! Form::label('foto', 'Foto:') !!}
    {!! Form::file('foto', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('kategori_id', 'Pilih Kategori:') !!}
    {!! Form::select('kategori_id', $kategori, null, ['class' => 'form-control', 'id' => 'select_kategori']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('provinces_id', 'Pilih Provinsi:') !!}
    {!! Form::select('provinces_id', $provinces, null, ['class' => 'form-control', 'id' => 'select_provinsi']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nama Yayasan:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Pemilik Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pemilik', 'Pemilik Yayasan:') !!}
    {!! Form::text('pemilik', null, ['class' => 'form-control']) !!}
</div>

<!-- No Izin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_izin', 'No Izin Yayasan:') !!}
    {!! Form::text('no_izin', null, ['class' => 'form-control']) !!}
</div>

<!-- Nama Rekening Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_rekening', 'Nama Rekening:') !!}
    {!! Form::text('nama_rekening', null, ['class' => 'form-control']) !!}
</div>

<!-- No Rekening Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_rekening', 'No Rekening:') !!}
    {!! Form::text('no_rekening', null, ['class' => 'form-control']) !!}
</div>

<!-- Nama Bank Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_bank', 'Nama Bank:') !!}
    {!! Form::text('nama_bank', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Yayasan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email_yayasan', 'Email Yayasan:') !!}
    {!! Form::text('email_yayasan', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('web', 'Website:') !!}
    {!! Form::text('web', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'No Telp Yayasan:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('alamat', 'Alamat:') !!}
    {!! Form::text('alamat', null, ['class' => 'form-control']) !!}
</div>

@role('superadmin')
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status :') !!}
    {!! Form::select('status', $status, null, ['class' => 'form-control', 'id' => 'select_status']) !!}
</div>
@endrole
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Simpan', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('backend.yayasan.index') }}" class="btn btn-default">Kembali</a>
</div>


@push('scripts')
    <script>
        $('#select_kategori').select2()
        $('#select_provinsi').select2()
        $('#select_status').select2()
    </script>
@endpush
