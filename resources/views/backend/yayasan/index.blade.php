@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Yayasan</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('backend.yayasan.create') }}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <form id="search-filter">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Range Tanggal:</label>
                                  <input type="text" name="filterdate" class="form-control" id="filterdate" placeholder="Pilih Tanggal">
                                  <input type="hidden" name="minDate" id="minDate">
                                  <input type="hidden" name="maxDate" id="maxDate">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Provinsi</label>
                                <select name="provinces_id" id="provinces_id" class="form-control" data-placeholder="Select ...">
                                    @foreach($provinsi as $provinsi)
                                        <option value=""></option>
                                        <option value="{{$provinsi->id}}">{{$provinsi->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" id="status" class="form-control" data-placeholder="Select ...">
                                    @foreach($status as $status)
                                        <option value=""></option>
                                        <option value="{{$status}}">{{$status}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Filter</label>
                                <div class="">
                                    <a class="btn btn-default" href="{{route('backend.yayasan.index')}}">Reset</a>
                                    <button type="submit" class="btn btn-success">Filter</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('backend.yayasan.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            var reqTable = $(".table").DataTable();
            $('#status').select2()
                $('#provinces_id').select2()
            
            $('#search-filter').on('submit', function (e) {
                
                reqTable.draw();
                e.preventDefault();
            });

            $('.custom-select-master').select2()
        });
    </script>
    
          
          
          
        
@endpush
