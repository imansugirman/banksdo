@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Yayasan</h1>
    </section>
    <div class="content">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row" style="padding: 20px">
                            @include('backend.yayasan.show_fields')
                            <a href="{{ route('backend.yayasan.index') }}" class="btn btn-default">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
