<img src={{ $yayasan->getFirstMediaUrl('foto_image') }} width="350" style="margin-bottom:30px">
<table class="table table-responsive table-bordered table-striped">
    <thead>
        <tr>
            <th>Field</th>
            <th>Data</th>
        </tr>    
    </thead>
    <tbody>
        <tr>
            <td>{!! Form::label('kategori_id', 'Kategori') !!}</td>
            <td>{{ $yayasan->kategori->name }}</td>
        </tr>
        <tr>
            <td>{!! Form::label('provinces_id', 'Provinsi') !!}</td>
            <td>{{ $yayasan->provinsi->name }}</td>
        </tr>
        <tr>
            <td>{!! Form::label('name', 'Nama Yayasan') !!}</td>
            <td>{{ $yayasan->name }}</td>
        </tr>
        <tr>
            <td>{!! Form::label('pemilik', 'Nama Pemilik Yayasan') !!}</td>
            <td>{{ $yayasan->pemilik }}</td>
        </tr>
        <tr>
            <td>{!! Form::label('no_izin', 'No Perizinan') !!}</td>
            <td>{{ $yayasan->no_izin }}</td>
        </tr>
        <tr>
            <td>{!! Form::label('nama_rekening', 'Nama Rekening Yayasan') !!}</td>
            <td>{{ $yayasan->nama_rekening }}</td>
        </tr>
        <tr>
            <td>{!! Form::label('no_rekening', 'No Rekening') !!}</td>
            <td>{{ $yayasan->no_rekening }}</td>
        </tr>
        <tr>
            <td>{!! Form::label('nama_bank', 'Nama Bank') !!}</td>
            <td>{{ $yayasan->nama_bank }}</td>
        </tr>
        <tr>
            <td>{!! Form::label('email_yayasan', 'Email') !!}</td>
            <td>{{ $yayasan->email_yayasan }}</td>
        </tr>
        <tr>
            <td>{!! Form::label('phone', 'No Telepon Yayasan') !!}</td>
            <td>{{ $yayasan->phone }}</td>
        </tr>
        <tr>
            <td>{!! Form::label('alamat', 'Alamat Yayasan') !!}</td>
            <td>{{ $yayasan->alamat }}</td>
        </tr>
    </tbody>
</table>
