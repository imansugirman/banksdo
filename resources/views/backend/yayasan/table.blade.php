@section('css')
    @include('layouts.datatables_css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

{!! $dataTable->table(['width' => '100%', 'class' => 'table table-striped table-bordered', 'id' => 'filterdatetable']) !!}

@push('scripts')
    @include('layouts.datatables_js')
    {!! $dataTable->scripts() !!}
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript">

    $('#filterdate').daterangepicker(
    	{
	      timePicker: false,
	      startDate: moment().startOf('date'), // ini ngaruh gak? kaga bang wkwkw
	      endDate: moment().startOf('date'),
	      opens: 'right',
        	locale: {
	            format: 'YYYY-MM-DD',
	            alwaysShowCalendars: true,
	            cancelLabel: 'Batal',
	            buttonClasses: ['btn btn-default'],
	            applyClass: 'applyBtn btn btn-sm btn-success',
	            cancelClass: 'btn-small',
	            fromLabel: 'Dari',
	            toLabel: 'Sampai',
	            weekLabel: 'W',
                	daysOfWeek: ['M','S','S','R','K','J','S'],
    		},
	    	ranges: {
	           'Hari Ini': [moment(), moment()],
	           'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	           '7 Hari Lalu': [moment().subtract(6, 'days'), moment()],
	           '30 Hari Lalu': [moment().subtract(29, 'days'), moment()],
	           'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
	           'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	    	},

    	}, 	function (startDate, endDate, label) {
	         $('#filterdate').on('apply.daterangepicker', function(e, picker) {
	          var minDate = startDate.format('YYYY-MM-DD'); // kalo gini gak jadi value. pake this val mas
	          var maxDate = endDate.format('YYYY-MM-DD');

	         $("#minDate").val(minDate)
	         $("#maxDate").val(maxDate)
   		});
   	});
    </script>
@endpush