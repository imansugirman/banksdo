@component('mail::message')
# Hey Admin

Ini adalah Notifikasi Web Bank Donation, ada pengguna mengupload yayasan

@component('mail::button', ['url' => 'https://bankdonation.com/login'])
Cek Sekarang
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
