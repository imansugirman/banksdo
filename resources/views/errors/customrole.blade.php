@extends('errors::layout-custom')

@section('title', __('Forbidden'))
@section('code', '403')
@section('message', __('Anda Tidak Diizinkan Mengakses Ini'))
