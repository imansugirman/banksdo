@extends('layouts.front')

@section('title', 'Bank Donation')

@section('content')
<main id="main">

    @foreach ($yayasan as $y)
    <div class="breadcrumbs" data-aos="fade-in">
      {{-- <div class="container">
        <h2>{{ $y->name }}</h2>
      </div> --}}
    </div><!-- End Breadcrumbs -->

    <section id="course-details" class="course-details">
      <div class="container" data-aos="fade-up">
        
        <div class="row">
          <div class="col-lg-6">
            <div class="card">
              
            
              <img src="{{ $y->getFirstMediaUrl('foto_image') }}" class="card-img-top" alt="">
              <div class="card-body text-center">
                <h5 class="card-title">{{ $y->name }}</h5>
              </div>
            </div>
            <h5 class="mt-3">Komentar</h5>
            @include('flash::message')
            @foreach ($comment as $record)
            <div class="card mt-3">
              <div class="card-body">
                {{ $record->comment }} - <span class="badge badge-primary text-wrap">{{ $record->created_at->format('d-m-Y') }}</span>
              </div>
            </div>
            @endforeach
            <div class="mt-3">
            {{ $comment->links() }}
            </div>

 {{--             {{ ->pluck('comment') }}
 --}}{{--             <div class="card">
 --}}              {!! Form::open(['route' => ['comment.store', $y->slug]]) !!}

              
              <div class="form-group mt-3">
                {!! Form::label('comment', 'Komentar') !!}
                {!! Form::textarea('comment', null, ['class' => 'form-control', 'rows' => 2, 'placeholder' => 'Isi Komentar Anda']) !!}
              </div>
              <div class="form-group">
                {!! Form::submit('Kirim', ['class' => 'btn btn-success']) !!}
              </div>
              
              {!! Form::close() !!}
{{--             </div>
 --}}          </div>
          <div class="col-lg-6">
          <div class="card">
            <table class="table">
                <tbody>
                  <tr>
                    <td>Nama Yayasan</td>
                    <td><a href="{{ url('/kategori', $y->kategori->slug) }}">{{ $y->name }}</a></td>
                  </tr>
                  <tr>
                    <td>Nama Pemilik Yayasan</td>
                    <td>{{ $y->pemilik }}</td>
                  </tr>
                  <tr>
                    <td>Provinsi Yayasan</td>
                    <td><a href="{{ url('/provinsi', $slug_provinsi) }}">{{ $y->provinsi->name }}</a></td>
                  </tr>
                  <tr>
                    <td>Kategori Yayasan</td>
                    <td><a href="{{ url('/kategori', $y->kategori->slug) }}">{{ $y->kategori->name }}</a></td>
                  </tr>
                  <tr>
                    <td>Nomor Perizinan</td>
                    <td>{{ $y->no_izin }}</td>
                  </tr>
                  <tr>
                    <td>Nomor Rekening Yayasan</td>
                    <td>{{ $y->no_rekening }}</td>
                  </tr>
                  <tr>
                    <td>Nama Rekening Yayasan</td>
                    <td>{{ $y->nama_rekening }}</td>
                  </tr>
                  <tr>
                    <td>Bank Yayasan</td>
                    <td>{{ $y->nama_bank }}</td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>{{ $y->email_yayasan }}</td>
                  </tr>
                  <tr>
                    <td>No Telepon</td>
                    <td>{{ $y->phone }}</td>
                  </tr>
                  <tr>
                    <td width="50%">Alamat</td>
                    <td width="50%">{{ $y->alamat }}</td>
                  </tr>
                  <tr>
                    <td>Website</td>
                    @if (!empty($y->web))
                      <td><a href="{{ $y->web }}">{{ $y->web }}</a></td>
                    @else
                      <td>-</td>
                    @endif
                  </tr>
                </tbody>
            </table>
            
              {{-- <div class="course-info d-flex justify-content-between align-items-center">
                <h5>Nama Yayasan</h5>
                <p><a href="{{ url('/kategori', $y->kategori->slug) }}">{{ $y->name }}</a></p>
              </div>

              <div class="course-info d-flex justify-content-between align-items-center">
                <h5>Nama Pemilik Yayasan</h5>
                <p>{{ $y->pemilik }}</p>
              </div> --}}

              {{-- <div class="course-info d-flex justify-content-between align-items-center">
                <h5>Provinsi Yayasan</h5>
                <a href="{{ url('/provinsi', $slug_provinsi) }}">{{ $y->provinsi->name }}</a>
              </div> --}}

{{--               <div class="course-info d-flex justify-content-between align-items-center">
                <h5>Kategori Yayasan</h5>
                <a href="{{ url('/kategori', $y->kategori->slug) }}">{{ $y->kategori->name }}</a>
              </div> --}}

              {{-- <div class="course-info d-flex justify-content-between align-items-center">
                <h5>Nomor Perizinan</h5>
                <p>{{ $y->no_izin }}</p>
              </div> --}}

{{--               <div class="course-info d-flex justify-content-between align-items-center">
                <h5>Nomor Rekening Yayasan</h5>
                <p>{{ $y->no_rekening }}</p>
              </div> --}}
{{-- 
              <div class="course-info d-flex justify-content-between align-items-center">
                <h5>Nama Rekening Yayasan</h5>
                <p>{{ $y->nama_rekening }}</p>
              </div> --}}
              
{{--               <div class="course-info d-flex justify-content-between align-items-center">
                <h5>Bank Yayasan</h5>
                <p>{{ $y->nama_bank }}</p>
              </div> --}}

{{--               <div class="course-info d-flex justify-content-between align-items-center">
                <h5>Email</h5>
                <p>{{ $y->email_yayasan }}</p>
              </div> --}}

              {{-- <div class="course-info d-flex justify-content-between align-items-center">
                <h5>Nomor Telepon</h5>
                <p>{{ $y->phone }}</p>
              </div> --}}

              {{-- <div class="course-info d-flex justify-content-between align-items-center">
                <h5>Alamat</h5>
                <p>{{ $y->alamat }}</p>
              </div> --}}
            </div>
          </div>
        </div>
        <h4 class="mt-5">Yayasan Lainnya</h4>
        <section id="popular-courses" class="courses">
          <div class="container" data-aos="fade-up">
          <div class="row">
              
              @foreach ($related as $related)
                  <div class="col-lg-3 col-md-6 align-items-stretch yayasan-grid-custom">
                    <div class="card border-left-blue">
                    <div class="course-item item-center text-center">
                      <img src="{{ $related->getFirstMediaUrl('foto_image', 'thumb') }}" class="img-fluid image-yayasan" alt="{{ $related->name }}">
                      <div class="course-content">
                        <h3 class="text-left"><a href="{{ url('/detail', $related->slug) }}">{{ Str::limit($related->name, 30) }}</a></h3>
                        <p>{{ Str::limit($related->alamat, 30) }}</p>
                        <div class="trainer justify-content-between align-items-center">
                          <div class="trainer-profile d-flex align-items-center">
                            <a class="btn btn-primary btn-block btn-sm" href="{{ url('/detail', $related->slug) }}">Selengkapnya</a>
                          </div>
                          
                        </div>
                      </div>
                    </div>
                    </div>
                  </div>
              @endforeach
              
          </div>
          </div>
        </section>
        

      </div>
    </section>

    @endforeach

  </main><!-- End #main -->
    
@endsection

