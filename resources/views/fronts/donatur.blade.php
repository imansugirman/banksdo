@extends('layouts.front')

@section('title', 'Bank Donation')

@section('content')
  <main id="main" data-aos="fade-in">
     <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs">
      {{--  <div class="container">
        <h2>Jadi Donatur</h2>
      </div>  --}}
    </div><!-- End Breadcrumbs -->

    <!-- ======= Courses Section ======= -->
    <section id="courses" class="courses">
      <div class="container" data-aos="fade-up">

        <div class="row justify-content-center" data-aos="zoom-in" data-aos-delay="100">

          <div class="col-lg-8 col-md-12 align-items-center">
            <div class="course-item">
              <div class="course-content">
                
                {!! Form::open(['route' => 'donaturstore', 'method' => 'post']) !!}
                  <div class="form-group">
                  {!! Form::label('nama_lengkap', 'Nama Lengkap :') !!}
                  {!! Form::text('nama_lengkap', null, ['class' => 'form-control']) !!}
                  </div>
                  <div class="form-group">
                  {!! Form::label('email', 'Email :') !!}
                  {!! Form::email('email', null, ['class' => 'form-control']) !!}
                  </div>
                  <div class="form-group">
                    {!! Form::label('phone', 'No Telepon :') !!}
                    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                  </div>
                  <div class="form-group">
                    {!! Form::label('asuransi', 'Sudah Punya Asuransi? :') !!}
                    {!! Form::select('asuransi', ['1' => 'Ya', '0' => 'Tidak'], null, ['class' => 'form-control']) !!}
                  </div>
                  <div class="form-group">
                    {!! Form::submit('Jadi Donatur', ['class' => 'btn btn-primary btn-block']) !!}
                  </div>
                  
                  
                  
                {!! Form::close() !!}
                
                
                {{--  <div class="d-flex justify-content-between align-items-center mb-3">
                  <h4>Web Development</h4>
                  <p class="price">$169</p>
                </div>

                <h3><a href="course-details.html">Website Design</a></h3>
                <p>Et architecto provident deleniti facere repellat nobis iste. Id facere quia quae dolores dolorem tempore.</p>
                <div class="trainer d-flex justify-content-between align-items-center">
                  <div class="trainer-profile d-flex align-items-center">
                    <img src="assets/img/trainers/trainer-1.jpg" class="img-fluid" alt="">
                    <span>Antonio</span>
                  </div>
                  <div class="trainer-rank d-flex align-items-center">
                    <i class="bx bx-user"></i>&nbsp;50
                    &nbsp;&nbsp;
                    <i class="bx bx-heart"></i>&nbsp;65
                  </div>
                </div>  --}}
              </div>
            </div>
          </div> 

        </div>

      </div>
    </section><!-- End Courses Section -->
  </main>  
@endsection

