@extends('layouts.front')

@section('content')


<section class="justify-content-center align-items-center" style="padding: 60px 0px 0px 0px">
  
  <div class="hero" data-arrows="true" data-autoplay="true">
    @foreach ($slides as $slide)
      <!--.hero-slide-->

      <div class="hero-slide">
        <img alt="Mars Image" class="img-responsive cover" src="{{ $slide->imageUrl('image') }}">
        <div class="header-content text-white position-absolute slide-content col-lg-4">
          <h1 class="mb-4">{{ $slide->title }}</h1>
            <h2 class="d-block font-weight-bold">{{ $slide->description }}</h2>
            @if (!empty($slide->text))
              <h2 class="d-block font-weight-bold">{{ $slide->text }}</h2>
            @endif
            @if (!empty($slide->link))
              <a class="btn btn-primary btn-lg w-max mt-2" href="{{ $slide->link }}" tabindex="0">Selengkapnya</a>

            @endif
        </div>
      </div>
      <!--.hero-slide-->
    @endforeach
  </div>
    {{-- <div class="owl-carousel owl-theme">
      
      

        <div class="item">
          <img src="" title="Title 1" alt="Alt 1" />
          
        </div>
        @endforeach

    </div> --}}


{{-- 
    <div class="container position-relative" data-aos="zoom-in" data-aos-delay="100">
      <h1>Layanan Kami</h1>
      <h2>Bank Donation adalah sumber informasi<br>
       kepada masyarakat serta calon donatur<br>
sebagai direktori yayasan dan lembaga sosial <br>
lainnya secara online di sekitar anda.
   
      </h2>
    </div> --}}
</section>
<main id="main">

    <section id="features" class="features custom-blue-background">
      <div class="container" data-aos="fade-up">

        <div class="row" data-aos="zoom-in" data-aos-delay="100">
          @foreach ($kategori as $kategori)
            <div class="col-lg-2 col-md-2 col-4 text-center">
                <a href="{{ url('/kategori', $kategori->slug) }}">
                  <img src="{{ $kategori->getFirstMediaUrl('icon_image') }}" class="mb-3"/>
                  <p class="text-white mobile-text-kategori">{{ $kategori->name }}</p>
                </a>
            </div>
          @endforeach

      </div>
    </section>
    <section id="popular-courses" class="courses custom-list-mobile-minus">
      <div class="container" data-aos="fade-up">
        <div class="row" data-aos="zoom-in" data-aos-delay="100">

          @foreach ($yayasan as $yay)
            <div class="col-6 col-md-3 align-items-stretch yayasan-grid-custom">
              <div class="card border-left-blue">
                <a href="{{ url('/detail', $yay->slug) }}">
                  <img src="{{ $yay->getFirstMediaUrl('foto_image', 'thumb') }}" class="card-img-top img-fluid custom-image-padding" alt="{{ $yay->name }}">
                </a>
                <div class="course-content">
                  <h3><a href="{{ url('/detail', $yay->slug) }}">{{ Str::limit($yay->name, 20) }}</a></h3>
                  <p>{{ Str::limit($yay->alamat, 30) }}</p>

                  <div class="trainer justify-content-between align-items-center">
                    <div class="trainer-profile d-flex align-items-center">
                      <a class="btn btn-primary btn-block btn-sm" href="{{ url('/detail', $yay->slug) }}">Selengkapnya</a>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          @endforeach



        </div>
        <div class="row mt-3 justify-content-center">
          <div class="text-center">
          {{ $yayasan->links() }}
          </div>

        </div>

      </div>
    </section>


  </main>
@endsection

@push('scripts')
  <script>
    $(document).ready(function ($) {
      $('.hero').slick({
      dots: true,
          infinite: true,
          speed: 500,
          fade: !0,
          cssEase: 'linear',
      slidesToShow: 1,
      slidesToScroll: 1,
          autoplay: true,
      autoplaySpeed: 3000,
          draggable: false,
      arrows: false,
      responsive: [
        {
      breakpoint: 1024,
      settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
          infinite: true
                }
          },
          {
      breakpoint: 768,
      settings: {
          draggable: true,
                }
      },
      {
      breakpoint: 600,
      settings: {
          slidesToShow: 1,
          draggable: true,
      slidesToScroll: 1
              }
      },
      {
      breakpoint: 480,
      settings: {
          slidesToShow: 1,
          draggable: true,
      slidesToScroll: 1
                }
      }
    
              ]
                    });
          });		


  </script>
@endpush

@section('css')
    <style>
      .hero-text h2 {
        margin-bottom: 50px;
      }
      
      .hero-text .hero {
        position: relative;
      }
      
      .hero-text .hero .hero-slide a:hover span {
        color: #033a71;
      }
      
      .hero .hero-slide img {
        background: rgba(0,0,0,.4);
        width: 100%;
        height: 600px;
        object-fit: cover;
        object-position: top center;
      }
      
      .hero .hero-slide .header-content {
        top: 15%;
        margin-left: 8rem;
        max-width: 550px;
        width: 100%;
        padding: 2rem;
      }
      
      .slide-content {
        padding: 10px 20px 10px 0;
      }
      
      .slide-content .h1 {
        font-size: 62px;
      }

      .slick-prev, .slick-next {
                top: auto;
                bottom: 175px;
                z-index: 10;
            }
            .slick-prev {
              left: 30px;
            
          }
          .slick-next {
            right: 30px;
        }
      
      
      
      
      /** Text Animation **/
      
      @-webkit-keyframes fadeInUpSD {
        0% {
          opacity: 0;
          -webkit-transform: translateY(100px);
          transform: translateY(100px);
        }
      
        100% {
          opacity: 1;
          -webkit-transform: none;
          transform: none;
        }
      }
      
      @keyframes fadeInUpSD {
        0% {
          opacity: 0;
          -webkit-transform: translateY(100px);
          transform: translateY(100px);
        }
      
        100% {
          opacity: 1;
          -webkit-transform: none;
          transform: none;
        }
      }
      
      .fadeInUpSD {
        -webkit-animation-name: fadeInUpSD;
        animation-name: fadeInUpSD;
      }
      
      .slick-active .slide-content {
        animation-name: fadeInUpSD;
        animation-duration: 1s;
        opacity: 1;
        width: 100%;
        padding: 10px 20px 30px 0;
      }
      
      /* Text Animation End **/
      
      .slick-dots {
        position: absolute;
        bottom: 170px;
        display: block;
        width: 100%;
        padding: 0;
        list-style: none;
        text-align: center;
      }
      
      .slick-dots li {
        position: relative;
        display: inline-block;
        width: 20px;
        height: 20px;
        margin: 0 5px;
        padding: 0;
        cursor: pointer;
      }
      
      .slick-active button {
        background: #3e9dfb;
      }
      
      .slick-dots li button {
        font-size: 0;
        line-height: 0;
        display: block;
        width: 20px;
        height: 20px;
        padding: 5px;
        cursor: pointer;
        border-radius: 50%;
        border: 0;
        outline: none;
      }
      
      .slick-dots li button::before {
        font-size: 18px;
        color: #fff;
        opacity: 1;
      }
      
      
      /* Media Queries */
      
      @media (max-width: 768px) {
        .hero-text .hero .hero-slide a {
          padding-top: 0.8rem;
        }
      
        .hero-text .hero .hero-slide a span {
          font-size: 20px;
          margin-top: 0.5rem;
        }
      
        .hero .hero-slide .header-content {
          left: 50%;
          -webkit-transform: translateX(-50%);
          transform: translateX(-50%);
          margin: 0 auto;
        }
      }
      
    </style>
@endsection