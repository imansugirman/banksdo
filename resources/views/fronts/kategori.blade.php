@extends('layouts.front')

@section('title', 'Bank Donation')

@section('content')

<main id="main">
    <section id="popular-courses" class="courses custom-list-pops">
      <div class="container" data-aos="fade-up">
        <div class="row" data-aos="zoom-in" data-aos-delay="100">

          @foreach ($yayasan as $yay)
            <div class="col-6 col-md-3 align-items-stretch yayasan-grid-custom">
              <div class="card border-left-blue">
                <a href="{{ url('/detail', $yay->slug) }}">
                  <img src="{{ $yay->getFirstMediaUrl('foto_image', 'thumb') }}" class="card-img-top img-fluid custom-image-padding" alt="{{ $yay->name }}">
                </a>
                <div class="course-content">
                  <h3><a href="{{ url('/detail', $yay->slug) }}">{{ $yay->name }}</a></h3>
                  <p>{{ Str::limit($yay->alamat, 30) }}</p>

                  <div class="trainer justify-content-between align-items-center">
                    <div class="trainer-profile d-flex align-items-center">
                      <a class="btn btn-primary btn-block btn-sm" href="{{ url('/detail', $yay->slug) }}">Selengkapnya</a>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          @endforeach
{{--
          @foreach ($yayasan as $yayasan)
            <div class="col-lg-3 col-md-6 align-items-stretch yayasan-grid-custom">
              <div class="course-item">
                <img src="{{ $yayasan->getFirstMediaUrl('foto_image', 'thumb') }}" class="img-fluid image-yayasan" alt="{{ $yayasan->name }}">
                <div class="course-content">
                  <h3><a href="{{ url('/detail', $yayasan->slug) }}">{{ $yayasan->name }}</a></h3>
                  <p>{{ $yayasan->alamat }}</p>
                  <div class="trainer justify-content-between align-items-center">
                    <div class="trainer-profile d-flex align-items-center">
                      <a class="btn btn-primary btn-block btn-sm" href="{{ url('/detail', $yayasan->slug) }}">Selengkapnya</a>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          @endforeach --}}

        </div>
          <div class="row mt-3 justify-content-center">
              <div class="text-center">
                  {{ $yayasan->links() }}
              </div>

          </div>

      </div>
    </section>


  </main>
@endsection

