<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bank Donation</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ mix('themes/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/select2.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/ionicons.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/themes.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/skins.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/icheck.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/custom.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
</head>
<body class="hold-transition register-page">
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            
    
        <div class="register-logo">
            <a href="{{ url('/home') }}"><img src="{{ asset('themes/img/logobankdonation.png')}}"></a>
        </div>

        <div class="register-box-body">
            <p class="login-box-msg">Daftar </p>
            <div class="bs-callout bs-callout-warning hidden">
              <h4>Oh snap!</h4>
              <p>This form seems to be invalid :(</p>
            </div>
            
            {!! Form::open(['route' => 'daftarsukses', 'method' => 'post', 'files' => true, 'enctype' => 'multipart/form-data']) !!}
                <div class="form-group">
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nama Lengkap']) !!}
                </div>
                <div class="form-group">
                    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                </div>
                <div class="form-group">
                    {!! Form::password('password', ['class' => 'form-control', 'label' => false, 'placeholder' => 'Password']) !!}
                </div>  
                <div class="form-group">
                    {!! Form::password('password_confirmation', ['class' => 'form-control', 'label' => false, 'placeholder' => 'Password Confirmation']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('roles', 'Daftar Sebagai :') !!}
                    {!! Form::select('roles', $roles, null,  ['class' => 'form-control', 'id' => 'select_roles']) !!}
                    
                </div>
                <div class="form-group">
                    {!! Form::label('foto', 'Foto:') !!}
                    {!! Form::file('foto', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('kategori_id', 'Pilih Kategori:') !!}
                    {!! Form::select('kategori_id', $kategori, null, ['class' => 'form-control', 'id' => 'select_kategori']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('provinces_id', 'Pilih Provinsi:') !!}
                    {!! Form::select('provinces_id', $provinces, null, ['class' => 'form-control', 'id' => 'select_provinsi']) !!}
                </div>

                <!-- Name Field -->
                <div class="form-group">
                    {!! Form::label('name', 'Nama Yayasan:') !!}
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Pemilik Field -->
                <div class="form-group">
                    {!! Form::label('pemilik', 'Pemilik Yayasan:') !!}
                    {!! Form::text('pemilik', null, ['class' => 'form-control']) !!}
                </div>

                <!-- No Izin Field -->
                <div class="form-group">
                    {!! Form::label('no_izin', 'No Izin Yayasan:') !!}
                    {!! Form::text('no_izin', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Nama Rekening Field -->
                <div class="form-group">
                    {!! Form::label('nama_rekening', 'Nama Rekening:') !!}
                    {!! Form::text('nama_rekening', null, ['class' => 'form-control']) !!}
                </div>

                <!-- No Rekening Field -->
                <div class="form-group">
                    {!! Form::label('no_rekening', 'No Rekening:') !!}
                    {!! Form::text('no_rekening', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Nama Bank Field -->
                <div class="form-group">
                    {!! Form::label('nama_bank', 'Nama Bank:') !!}
                    {!! Form::text('nama_bank', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Email Yayasan Field -->
                <div class="form-group">
                    {!! Form::label('email_yayasan', 'Email Yayasan:') !!}
                    {!! Form::text('email_yayasan', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('web', 'Website:') !!}
                    {!! Form::text('web', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Phone Field -->
                <div class="form-group">
                    {!! Form::label('phone', 'No Telp Yayasan:') !!}
                    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('alamat', 'Alamat:') !!}
                    {!! Form::text('alamat', null, ['class' => 'form-control']) !!}
                </div>

    {{--             @role('superadmin')
                <div class="form-group col-sm-6">
                    {!! Form::label('status', 'Status :') !!}
                    {!! Form::select('status', $status, null, ['class' => 'form-control', 'id' => 'select_status']) !!}
                </div>
                @endrole --}}
                <!-- Submit Field -->
    {{--             <div class="form-group col-sm-12">
                    {!! Form::submit('Simpan', ['class' => 'btn btn-primary']) !!}
                    <a href="{{ route('backend.yayasan.index') }}" class="btn btn-default">Kembali</a>
                </div> --}}


                

                
                <div class="form-group">
                    {!! Form::submit('Daftar Sekarang', ['class' => 'btn btn-primary btn-block']) !!}
                </div>            
            {!! Form::close() !!}
            
            {{-- <a class="btn btn-success btn-block" href="{{ url('/login') }}" class="text-center">Sudah Punya Akun</a> --}}
            
                <hr>
            </div>
        </div>
    </div>
</div>
@push('scripts')
                    <script>
                        $('#select_kategori').select2()
                        $('#select_provinsi').select2()
                        $('#select_status').select2()
                    </script>
                @endpush
<script src="{{ mix('themes/js/jquery.min.js')}}"></script>
<script src="{{ mix('themes/js/bootstrap.min.js')}}"></script>
<script src="{{ mix('themes/js/themes.min.js')}}"></script>
<script src="{{ mix('themes/js/icheck.min.js')}}"></script>
<script src="{{ mix('themes/js/select2.min.js')}}"></script>
<script src="{{ mix('themes/js/parsley.min.js')}}"></script>


<script>
    
    $('#select_roles').select2()
</script>
</body>
</html>
