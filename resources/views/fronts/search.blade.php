@extends('layouts.front')

@section('title', 'Bank Donation')

@section('content')
<section class="d-flex justify-content-center align-items-center">
      
</section>
<main id="main">
    <section id="popular-courses" class="courses">
      <div class="container" data-aos="fade-up">
        <div class="row" data-aos="zoom-in" data-aos-delay="100">
            <div class="col-md-12 col-lg-12 text-center">
              <h4>Hasil Pencarian dari kata kunci : {{ $q }}</h4>
            </div>
          @foreach ($yayasan as $yayasan)
            <div class="col-lg-3 col-md-6 align-items-stretch yayasan-grid-custom">
              <div class="course-item">
                <img src="{{ $yayasan->getFirstMediaUrl('foto_image', 'thumb') }}" class="img-fluid" alt="{{ $yayasan->name }}">
                <div class="course-content">
                  <h3><a href="{{ url('/detail', $yayasan->slug) }}">{{ $yayasan->name }}</a></h3>
                  <p>{{ $yayasan->alamat }}</p>
                  <div class="trainer justify-content-between align-items-center">
                    <div class="trainer-profile d-flex align-items-center">
                      <a class="btn btn-primary btn-block btn-sm" href="{{ url('/detail', $yayasan->slug) }}">Selengkapnya</a>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          @endforeach

        </div>

      </div>
    </section>


  </main>
@endsection

