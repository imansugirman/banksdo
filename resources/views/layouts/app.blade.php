<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Bank Donation</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('themes/img/iconb.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('themes/img/iconm.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('themes/img/icons.png') }}">

    <link rel="stylesheet" href="{{ mix('themes/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/bootstrap-toggle.min.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/select2.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/themes.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/skins.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/icheck.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/icons.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/ionicons.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ mix('themes/css/custom.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">


    @yield('css')
</head>

<body class="skin-purple-light sidebar-mini">
@if (!Auth::guest())
    <div class="wrapper">
        <header class="main-header">
            <a href="{{ url('/home') }}" class="logo">
                <img src="{{ asset('themes/img/logobankdonationwhite.png')}}" width="60"><b>Bank Donation</b>
            </a>

            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown tasks-menu">
                            <a href="{{ url('/')}}" class="dropdown-toggle">
                            <i class="fa fa-globe"></i>
                            Front Web</a>
                        </li>
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                @if (empty(Auth::user()->getFirstMediaUrl('avatars')))
                                    <img src="{{ asset('themes/img/user_avatar.png')}}" class="user-image" alt="User Image" />
                                @else
                                    <img src="{{ Auth::user()->getFirstMediaUrl('avatars') }}" class="user-image" alt="User Image"/>
                                @endif
                                <span class="hidden-xs">{{ Auth::user()->name }}</span>
                            </a>
                            <ul class="dropdown-menu">
                                
                                <li class="user-header">
                                    @if (empty(Auth::user()->getFirstMediaUrl('avatars')))
                                        <img src="{{ asset('themes/img/user_avatar.png')}}" class="img-circle" alt="User Image" />
                                    @else
                                        <img src="{{ Auth::user()->getFirstMediaUrl('avatars') }}" class="img-circle" alt="User Image"/>
                                    @endif
                                    <p>
                                        {{ Auth::user()->name }}
                                        <small>Member since {{ Auth::user()->created_at->format('M. Y') }}</small>
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ url('/logout') }}" class="btn btn-default btn-flat"
                                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Sign out
                                        </a>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- Left side column. contains the logo and sidebar -->
        @include('layouts.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>

        <footer class="main-footer" style="max-height: 100px;text-align: center">
            <strong>Copyright © 2020 <a href="#">Bank Donation</a>.</strong> All rights reserved.
        </footer>

    </div>
@else
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">
                    Bank Donation
                </a>
            </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/home') }}">Home</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    @endif
    <script src="{{ mix('themes/js/jquery.min.js')}}"></script>
    <script src="{{ mix('themes/js/moment.min.js')}}"></script>
    <script src="{{ mix('themes/js/bootstrap.min.js')}}"></script>
    <script src="{{ mix('themes/js/bootstrap-datetimepicker.js')}}"></script>
    <script src="{{ mix('themes/js/bootstrap-toggle.min.js')}}"></script>
    <script src="{{ mix('themes/js/themes.min.js')}}"></script>
    <script src="{{ mix('themes/js/icheck.min.js')}}"></script>
    <script src="{{ mix('themes/js/select2.min.js')}}"></script>
    @stack('scripts')
</body>
</html>