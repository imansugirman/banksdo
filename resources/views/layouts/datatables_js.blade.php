<script src="{{ mix('themes/js/jquery.datatables.min.js') }}"></script>
<script src="{{ mix('themes/js/datatables.bootstrap.min.js') }}"></script>
<script src="{{ mix('themes/js/datatables.buttons.min.js') }}"></script>
<script src="{{ mix('themes/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ mix('themes/js/buttons.colvis.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>