<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Bank Donation</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('themes/img/iconb.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('themes/img/iconm.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('themes/img/icons.png') }}">
    <link rel="stylesheet" href="{{ mix('front/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ mix('front/css/icofont.min.css') }}">
    <link rel="stylesheet" href="{{ mix('front/css/boxicons.min.css') }}">
    <link rel="stylesheet" href="{{ mix('front/css/remixicon.css') }}">
    {{-- <link rel="stylesheet" href="{{ mix('front/css/owl.css') }}"> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" integrity="sha512-yHknP1/AwR+yx26cB1y0cjvQUMvEa2PFzt1c9LlS4pRQ5NOTZFWbhBig+X9G9eYW/8m0/4OXNx8pxJ6z57x0dw==" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{ mix('front/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ mix('front/css/aos.css') }}">
    <link rel="stylesheet" href="{{ mix('front/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ mix('front/css/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ mix('front/css/style.min.css') }}">
    <link rel="stylesheet" href="{{ mix('front/css/custom.min.css') }}">


    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
    @yield('css')
</head>
<body>
    @include('shared.header')
    

    @yield('content')
    @include('shared.footer')

    <a href="#" class="back-to-top"><i class="bx bx-up-arrow-alt"></i></a>
    <div id="preloader"></div>


    <script src="{{ mix('front/js/jquery.min.js')}}"></script>
    <script src="{{ mix('front/js/bootstrap.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous"></script>    <script src="{{ mix('front/js/jquery.easing.min.js')}}"></script>
    <script src="{{ mix('front/js/jquery.waypoints.min.js')}}"></script>
    <script src="{{ mix('front/js/counterup.min.js')}}"></script>
    <script src="{{ mix('front/js/owl.carousel.min.js')}}"></script>
    <script src="{{ mix('front/js/aos.js')}}"></script>
    <script src="{{ mix('front/js/select2.min.js') }}"></script>

    <script src="{{ asset('front/js/main.js')}}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
      $('#select_sol').select2({
        placeholder: 'Indonesia',
        theme: 'bootstrap4',        
      }).on('select2:select', function(e){
            var data = e.params.data;
            var selected = $('#select_sol').val();
            var provinces_id = data.id
            console.log(data.id + 'ok' + selected + 'pro' + provinces_id);
            $('#search').submit();

      });

      $('#search_prov').select2({
        placeholder: 'Indonesia',
        theme: 'bootstrap4',
        width: '100%'        
      }).on('select2:select', function(e){
            var data = e.params.data;
            var selected = $('#search_prov').val();
            var provinces_id = data.id
            console.log(data.id + 'ok' + selected + 'pro' + provinces_id);
            $('#provid').submit();

      });

      

    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    @stack('scripts')

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-176402206-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-176402206-1');
    </script>

</body>

</html>