@role('superadmin')
<li class="treeview">
	<a href="#"><i class="ic-users"></i> <span>User Manager</span>
		<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
	</a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('backend/users*') ? 'active' : '' }}">
			<a href="{{ route('backend.users.index') }}"><i class="ic-users"></i> <span>Users</span></a>
		</li>

		<li class="{{ Request::is('backend/roles*') ? 'active' : '' }}">
		    <a href="{{ route('backend.roles.index') }}"><i class="ic-shield1"></i> <span>Roles</span></a>
		</li>

		<li class="{{ Request::is('backend/permissions*') ? 'active' : '' }}">
		    <a href="{{ route('backend.permissions.index') }}"><i class="ic-lock_outline"></i> <span>Permissions</span></a>
		</li>
    </ul>
</li>


<li class="{{ Request::is('backend/kategori*') ? 'active' : '' }}">
    <a href="{{ route('backend.kategori.index') }}"><i class="ic-now_widgets"></i> <span>Kategori</span></a>
</li>

<li class="{{ Request::is('backend/provinsi*') ? 'active' : '' }}">
    <a href="{{ route('backend.provinsi.index') }}"><i class="ic-location_on"></i> <span>Provinsi</span></a>
</li>
<li class="{{ Request::is('backend/konfirmasi*') ? 'active' : '' }}">
    <a href="{{ route('backend.konfirmasi.index') }}"><i class="ic-event_note"></i> <span>Konfirmasi Pembayaran</span></a>
</li>
@endrole
@role('yayasan|superadmin')

<li class="{{ Request::is('backend/yayasan*') ? 'active' : '' }}">
    <a href="{{ route('backend.yayasan.index') }}"><i class="ic-codesandbox"></i> <span>Yayasan</span></a>
</li>
@endrole


<li class="{{ Request::is('backend/donatur*') ? 'active' : '' }}">
    <a href="{{ route('backend.donatur.index') }}"><i class="ic-codesandbox"></i> <span>Donatur</span></a>
</li>

<li class="{{ Request::is('backend/comments*') ? 'active' : '' }}">
    <a href="{{ route('backend.comments.index') }}"><i class="fa fa-edit"></i><span>Comments</span></a>
</li>

<li class="{{ Request::is('backend/slide*') ? 'active' : '' }}">
    <a href="{{ route('backend.slide.index') }}"><i class="fa fa-edit"></i><span>Slide</span></a>
</li>

