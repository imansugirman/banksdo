<footer id="footer">
      <div class="footer-top">
        <div class="container">
          <div class="row justify-content-between">

            <div class="col-lg-7 col-md-6 footer-contact ">
              <h3>Tentang Bank Donation</h3>
              <p class="footer-white">
                Bank Donation adalah Marketplace khusus bagi Yayasan<br>
dengan Donatur yang ingin memberikan dampak sosial secara aman dan<br>
transparan tanpa ada potongan biaya. Melalui donasi online, Bank Donation<br>
hadir menjembatani keduanya agar dapat bergandeng tangan untuk<br>
menciptakan dampak baik bagi Indonesia.<br>
              </p>
            </div>

            <div class="col-lg-5 col-md-6 footer-newsletter">
              <h3>Kantor Pusat & Pelayanan</h3>
              <p>Graha Morzell<br>
              Jl. Pejaten Raya Jl. Raya Pasar Minggu No.22, RT.13/RW.2, Jati Padang<br>
              Kec. Ps.Minggu, Kota Jakarta Selatan<br>
              Daerah Khusus Ibukota Jakarta 12540<br>
              Kontak :  <a href="mailto:admin@bankdonation.com">admin@bankdonation.com</a><br>
              Telepon : (021) 79195711 – 79195724<br>
              Fax : (021) 79195717<br>
              Mobile : +62 851-5501-3588<br>
              </p>
            </div>

          </div>
        </div>
      </div>
      <div class="container d-md-flex pb-4">
        <div class="mr-auto social-links text-left text-md-left">
          <h4>Ikuti Kami</h4>
          <a href="https://facebook.com/bankdonation" class="facebook"><i class="bx bxl-facebook"></i></a>
          <a href="https://www.instagram.com/bankdonation" class="instagram"><i class="bx bxl-instagram"></i></a>
          <a href="#" class="google-plus"><i class="bx bxl-youtube"></i></a>
          <a href="https://wa.me/6281386907612" class="google-whatsapp"><i class="bx bxl-whatsapp"></i></a>
        </div>
        <div class="col-lg-5 text-md-left pt-3 pt-md-0">
            <div class="copyright">
              &copy; Copyright <strong><span>BankDonation.com</span></strong>. All Rights Reserved
            </div>
          <div class="credits">
          </div>
        </div>
      </div>
  </footer>