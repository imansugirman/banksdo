<header id="header" class="fixed-top">
        <div class="container mobile-cont d-flex align-items-center">
          <a href="{{ url('/') }}" class="logo mr-auto"><img src="{{ asset('themes/img/logobankdonation.png')}}" alt="" class="img-fluid"></a>

          <div class="d-block d-sm-none col">
                <form action="{{ route('searchprovinsi') }}" id="provid">
                        <select name="id" class="my-1 search_prov" id="search_prov">
                          <option value="">Indonesia</option>
                          @foreach ($provinsimobile as $prov)
                            <option value="{{ $prov->id }}">{{ $prov->name }}</option>
                          @endforeach
                        </select>
                </form>
          </div>

          <nav class="nav-menu d-none d-lg-block mr-auto d-sm-block">
              <div class="form-inline">
                  <div class="form-row">
                      <div class="form-group">
                <form action="{{ route('searchprovinsi') }}" id="search">
                  <div class="input-group mr-4">
                        <div class="input-group-prepend">
                          <span class="input-group-text custom-search-lokasi"><i class='bx bx-search'></i></span>
                        </div>
                        <select name="id" class="my-1 mr-sm-2" id="select_sol">
                          <option value="">Indonesia</option>
                          @foreach ($provinsi as $provinsi)
                            <option value="{{ $provinsi->id }}">{{ $provinsi->name }}</option>
                          @endforeach
                        </select>
                  </div>
                </form>
                <form action="{{ url('search') }}">
                  <div class="input-group mr-0">
                        <input type="text" class="form-control custom-search-header" name="q" placeholder="Temukan Yayasan">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button">
                                <i class='bx bx-search'></i>
                            </button>
                        </div>

                  </div>
                </form>
                      </div>
                  </div>
              </div>
              {{-- <input type="search" class="form-control" name="search" id="" placeholder="Temukan Yayasan"> --}}
            <ul>

          </nav>

          <a href="{{ route('daftar') }}" >
            <img class="p-2 img-mobile-upload image-upload-yayasan" src="{{ asset('themes/img/uploadyayasan.png') }}" alt="">
          </a>
{{--           <a href="{{ route('donatur') }}" >
            <img class="p-2 img-mobile-upload image-upload-yayasan" src="{{ asset('themes/img/jadidonaturicon.png') }}" alt="">
          </a> --}}
        </div>
        <div class="d-block d-sm-none">


            <div class="mt-2">
              <form action="{{ url('search') }}">
                <div class="input-group mr-0">
                      <input type="text" class="form-control custom-search-header custom-search-mobile" name="q" placeholder="Temukan Yayasan">
                      <div class="input-group-append">
                          <button class="btn btn-primary btn-custom-kotak" type="button">
                              <i class='bx bx-search'></i>
                          </button>
                      </div>

                </div>
              </form>
            </div>
        </div>
    </header>
