<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'FrontController@index')->name('front');
Route::get('/kategori/{slug}', 'FrontController@kategori')->name('kategori');
Route::get('/provinsi/{slug}', 'FrontController@provinsi')->name('provinsi');

Route::get('/search', 'FrontController@search');
Route::get('/searchprovinsi', 'FrontController@searchProvinsi')->name('searchprovinsi');


Route::get('/searchmobile', 'FrontController@search');
Route::get('/searchprovinsimobile', 'FrontController@searchProvinsi')->name('searchprovinsimobile');

Route::get('/detail/{slug}', 'FrontController@detail')->name('detail');
Route::post('/detail/{slug}', 'FrontController@comment')->name('comment.store');

Route::get('/donatur', 'FrontController@donatur')->name('donatur');
Route::post('/donatur', 'FrontController@donaturstore')->name('donaturstore');

Route::get('/daftar', 'RegistrasiController@index')->name('daftar');
Route::post('/daftar', 'RegistrasiController@store')->name('daftarsukses');
// Route::get('/daftar', 'RegistrasiController@index')->name('daftar');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Auth::routes(['verify' => true]);

// Route::get('/home', 'HomeController@index')->middleware('verified');

// Route::group(['prefix' => 'backend', 'middleware' => ['auth', 'role:super-admin']], function () {

Route::group(['prefix' => 'backend', 'middleware' => ['auth', 'role:superadmin|admin|yayasan']], function () {
    Route::resource('users', 'Backend\UserController', ["as" => 'backend']);
    Route::resource('roles', 'Backend\RoleController', ["as" => 'backend']);
    Route::resource('permissions', 'Backend\PermissionController', ["as" => 'backend']);
    Route::resource('kategori', 'Backend\KategoriController', ["as" => 'backend']);
    Route::resource('provinsi', 'Backend\ProvinsiController', ["as" => 'backend']);
    Route::resource('konfirmasi', 'Backend\KonfirmasiController', ["as" => 'backend']);
    Route::resource('donatur', 'Backend\DonaturController', ["as" => 'backend']);
    Route::resource('comments', 'Backend\CommentsController', ["as" => 'backend']);
    Route::resource('slide', 'Backend\SlideController', ["as" => 'backend']);

});

Route::group(['prefix' => 'backend', 'middleware' => ['auth', 'role:yayasan|superadmin|admin']], function () {
    Route::resource('yayasan', 'Backend\YayasanController', ["as" => 'backend']);
});




Route::group(['prefix' => 'backend'], function () {
});
